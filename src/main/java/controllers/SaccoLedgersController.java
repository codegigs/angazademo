package controllers;

import controllers.util.AppConstants;
import controllers.util.JsfUtil;
import controllers.util.JsfUtil.PersistAction;
import entities.ExpensesCategory;
import entities.SaccoLedgers;
import facades.SaccoLedgersFacade;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Named;
import org.joda.time.DateTime;

@Named("saccoLedgersController")
@SessionScoped
public class SaccoLedgersController implements Serializable {

    @EJB
    private facades.SaccoLedgersFacade ejbFacade;
    private List<SaccoLedgers> items = null;
    private SaccoLedgers selected;

    public SaccoLedgersController() {
    }

    public SaccoLedgers getSelected() {
        return selected;
    }

    public void setSelected(SaccoLedgers selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private SaccoLedgersFacade getFacade() {
        return ejbFacade;
    }

    public SaccoLedgers prepareCreate() {
        selected = new SaccoLedgers();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("SaccoLedgersCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("SaccoLedgersUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("SaccoLedgersDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void copy()
    {
        for(SaccoLedgers sl:getItems())
        {
            ExpensesCategory exp=new ExpensesCategory();
            exp.setDescription("");
            exp.setTitle(sl.getName());
            getFacade().storeE(exp);
        }
    }
    public void moveAll()
    {
        for(SaccoLedgers sl:getItems())
        {
            AppConstants.logger("merged");
            sl.setId(sl.getId()+50);
            getFacade().editSl(sl);
        }
    }
    public List<SaccoLedgers> getItems() {
        items = getFacade().findAll();
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }

    public SaccoLedgers getSaccoLedgers(java.lang.Integer id) {
        return getFacade().find(id);
    }

    public List<SaccoLedgers> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<SaccoLedgers> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    @FacesConverter(forClass = SaccoLedgers.class)
    public static class SaccoLedgersControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            SaccoLedgersController controller = (SaccoLedgersController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "saccoLedgersController");
            return controller.getSaccoLedgers(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof SaccoLedgers) {
                SaccoLedgers o = (SaccoLedgers) object;
                return getStringKey(o.getId());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), SaccoLedgers.class.getName()});
                return null;
            }
        }

    }
    public int getTotalCredits()
    {
        int total=0;
        if(getItems()!=null)
        {
           for(SaccoLedgers sl:getItems())
           {
               total+=sl.getCr();
           }
        }
        return total;
    }
    public int getTotalDebits()
    {
        int total=0;
        if(getItems()!=null)
        {
           for(SaccoLedgers sl:getItems())
           {
               total+=sl.getDr();
           }
        }
        return total;
    }

    public String getTitle() {
        return "TKN Sacco Trial Balance As At " + new SimpleDateFormat("MMMM-yyyy").format(new DateTime().minusMonths(1).toDate()).replace("-", " Year ");
    }
}
