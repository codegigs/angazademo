package controllers;

import controllers.util.AppConstants;
import controllers.util.JsfUtil;
import controllers.util.JsfUtil.PersistAction;
import entities.AccountStatus;
import entities.AccountTransitions;
import entities.BillTransactions;
import entities.CollectionTypes;
import entities.Collections;
import entities.DepositCash;
import entities.Guarantors;
import entities.Loan;
import entities.LoanRecovery;
import entities.LoanScheduleTransactions;
import entities.LoanSchedules;
import entities.LoanTypes;
import entities.MemberBills;
import entities.Members;
import entities.PaymentModes;
import entities.PaymentSummary;
import entities.PaymentSummaryItems;
import entities.SaccoLedgerEntries;
import entities.SaccoLedgers;
import entities.ScheduleStatus;
import entities.SystemSettings;
import entities.Withdrawal;
import facades.MembersFacade;
import java.io.IOException;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.concurrent.Executor;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.joda.time.DateTime;
import org.primefaces.event.RowEditEvent;

@Named("membersController")
@SessionScoped
public class MembersController implements Serializable {

    private static ArrayList<LoanSchedules> list;

    @EJB
    private facades.MembersFacade ejbFacade;
    private List<Members> items = null;
    private Members selected;
    private int entryfee, registrationfee;
    private DepositCash depo = new DepositCash();
    private Loan loan = new Loan();
    private Date sdate = new Date(), edate = new Date(), lastUpdated;
    private int InterestRate, negotiation, withdrawalcharges, gracePeriod;
    private LoanScheduleTransactions itemtransaction = new LoanScheduleTransactions();
    private int noOfGuarantors = 5;
    private ArrayList<Guarantors> listg = new ArrayList<>();
    private PaymentSummary p;
    private CollectionTypes type;
    private FacesContext faceContext;

    @PostConstruct
    public void loadProfits() {
        faceContext = FacesContext.getCurrentInstance();
        SystemSettings st = getFacade().getSettings(1);
        entryfee = Integer.parseInt(st.getValue());
        st = getFacade().getSettings(2);
        registrationfee = Integer.parseInt(st.getValue());
        st = getFacade().getSettings(3);
        InterestRate = Integer.parseInt(st.getValue());
        st = getFacade().getSettings(4);
        negotiation = Integer.parseInt(st.getValue());
        //st = getFacade().getSettings(5);
        //loanformcharges = Integer.parseInt(st.getValue());
        st = getFacade().getSettings(6);
        withdrawalcharges = Integer.parseInt(st.getValue());
        st = getFacade().getSettings(8);
        emegencyinterestrate = Integer.parseInt(st.getValue());
        //st = getFacade().getSettings(9);
        //gracePeriod = Integer.parseInt(st.getValue());
        st = getFacade().getSettings(7);
        try {
            lastUpdated = new SimpleDateFormat("yyyy-MM-dd").parse(st.getValue());
        } catch (ParseException ex) {
            Logger.getLogger(MembersController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public CollectionTypes getType() {
        return type;
    }

    public void setType(CollectionTypes type) {
        this.type = type;
    }

    @Inject
    private Executor executor;
    private int emegencyinterestrate;

    public int getNoOfGuarantors() {
        return noOfGuarantors;
    }

    public List<DepositCash> getAllDeposits() {
        return getFacade().getAllMembersDeposits(selected);
    }

    public ArrayList<Guarantors> getGuarators() {
        if (listg.isEmpty()) {
            for (int x = 0; x < noOfGuarantors; x++) {
                Guarantors g = new Guarantors();
                g.setId(x);
                g.setGuaranteedAmount(0);
                listg.add(g);
            }
            return listg;
        } else {
            return listg;
        }
    }

    public ArrayList<Guarantors> getGuaratorsForTheLoan() {
        ArrayList<Guarantors> loanGuarantor = new ArrayList<>();
        for (Guarantors g : loan.getGuarantorsCollection()) {
            loanGuarantor.add(g);
            AppConstants.logger("G id:" + g.getId());
        }
        return loanGuarantor;
    }

    public void prepareCreateGuarantor() {
        listg = getGuaratorsForTheLoan();
    }

    public void createLoan() {
        if (loan.getAmount() == 0) {
            FacesContext.getCurrentInstance().addMessage(
                    null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Loan has 0 amount", null));
            return;
        }
        if (loan.getLoantype().getId() == 1) {
            loan.setInterestRate(emegencyinterestrate);
        } else {
            loan.setInterestRate(InterestRate);
        }
        if (loan.getLoantype().getId() == 1 && selected.getActiveLoanEmergency() != null) {
            if (selected.getActiveLoanEmergency().getBalEst() == 0) {
                loan.setPedding(true);
            } else {
                AppConstants.logger("member has an emergency loan. dont create");
                FacesContext.getCurrentInstance().addMessage(
                        null,
                        new FacesMessage(FacesMessage.SEVERITY_WARN,
                                "Member has an active emergency loan", null));
                return;
            }
        }
        if (loan.getLoantype().getId() == 2 && selected.getActiveLoanNormal() != null) {
            if (selected.getActiveLoanNormal().getBalEst() == 0) {
                loan.setPedding(true);
            } else {
                AppConstants.logger("member has an normal loan. dont create");
                FacesContext.getCurrentInstance().addMessage(
                        null,
                        new FacesMessage(FacesMessage.SEVERITY_WARN,
                                "Member has an active normal loan", null));
                return;
            }
        }

        int random = new Random().nextInt(10000);
        loan.setRandomInt(random);
        loan.setGracePeriodCount(gracePeriod);
        getFacade().storeLoan(loan);
        loan = getFacade().getLoanByRandom(random);
        loan.setRandomInt(-1);
        getFacade().storeLoan(loan);
        SaccoLedgerEntries sle = new SaccoLedgerEntries();
        sle.setCr(0);
        sle.setDateWhen(new Date());
        sle.setDr((int) loan.getAmount());
        sle.setLedgerId(new SaccoLedgers(22));
        sle.setIsGeneral(false);
        sle.setParticulars("members loans to " + loan.getMemeberId().getSurname());
        getFacade().saveLedgerEntry(sle);
        sle = new SaccoLedgerEntries();
        sle.setCr((int) loan.getAmount());
        sle.setDateWhen(new Date());
        sle.setDr(0);
        sle.setIsGeneral(false);
        sle.setParticulars("members loans to " + loan.getMemeberId().getSurname());
        sle.setLedgerId(new SaccoLedgers(7));
        getFacade().saveLedgerEntry(sle);
        int interest = (int) (loan.getAmount() * loan.getInterestRate() / 12 * 0.01);
        LoanSchedules item = new LoanSchedules();
        item.setPrincipleAmount((float) loan.getAmount());
        item.setInterest(interest);
        item.setBalance((float) loan.getAmount());
        item.setLoanId(loan);
        item.setStatusCleared(new ScheduleStatus(2));
        item.setRandomInt(random);
        DateTime today = new DateTime();
        DateTime nextAfterAMonth = today.plusMonths(1);
        Date objectDate = nextAfterAMonth.toDate();
        item.setDueDate(objectDate);
        item.setIsForLastMonth(true);
        getFacade().storeLoanSchedule(item);
        item = getFacade().getLoanSchedule(random);
        item.setRandomInt(-1);
        getFacade().mergeLoanSchedule(item);
        LoanScheduleTransactions lst = new LoanScheduleTransactions();
        lst.setAmountDebit(interest);
        lst.setAmountCredit(0.0);
        lst.setDateCollected(new Date());
        lst.setDetails("interest");
        lst.setScheduleId(item);
        getFacade().storeTransaction(lst);
        if (negotiation > 0) {
            lst.setAmountDebit(loan.getAmount() * negotiation / 100);
            lst.setDetails("loannegotiation");
            getFacade().storeTransaction(lst);
            loan.setNegotiationFee((int) Math.ceil(loan.getAmount() * negotiation / 100));
            getFacade().storeLoan(loan);
//            sle = new SaccoLedgerEntries();
//            sle.setCr((int) Math.ceil(loan.getAmount() * negotiation / 100));
//            sle.setDateWhen(new Date());
//            sle.setDr(0);
//            sle.setLedgerId(new SaccoLedgers(44));
//            getFacade().saveLedgerEntry(sle);
//            sle = new SaccoLedgerEntries();
//            sle.setCr(0);
//            sle.setDateWhen(new Date());
//            sle.setDr((int) Math.ceil(loan.getAmount() * negotiation / 100));
//            sle.setLedgerId(new SaccoLedgers(6));
//            getFacade().saveLedgerEntry(sle);
        }
        if (loan.getMemeberId().getTotalDepositWithrawable() > 0) {
//            lst.setAmountCredit((double) loan.getMemeberId().getTotalDepositWithrawable());
//            lst.setAmountDebit(0.0);
//            lst.setDateCollected(new Date());
//            lst.setDetails("paid from deposits");
//            lst.setScheduleId(item);
//            getFacade().storeTransaction(lst);
            Collections c = new Collections();
            c.setAmount(loan.getMemeberId().getTotalDepositWithrawable());
            c.setDateCollected(new Date());
            c.setMemberId(loan.getMemeberId());
            c.setReceiptNo(11111);
            c.setCollectionstype(new CollectionTypes(loan.getLoantype().getId() + 1));
            getFacade().storeCollections(c);

            DepositCash depo = new DepositCash();
            depo.setAmount(-loan.getMemeberId().getTotalDepositWithrawable());
            depo.setChequeNo("2222");
            depo.setDate(new Date());
            depo.setDescription("paid loans");
            depo.setIsWithdrawable(true);
            depo.setTransactedBy("jackie");
            depo.setPaymentMode(new PaymentModes(1));
            depo.setMemberNo(loan.getMemeberId());
            getFacade().storeDeposit(depo);

            sle = new SaccoLedgerEntries();
            sle.setDateWhen(new Date());
            sle.setCr(depo.getAmount() * -1);
            sle.setDateWhen(new Date());
            sle.setDr(0);
            sle.setParticulars("Loan Recovery");
            sle.setIsGeneral(true);
            sle.setLedgerId(new SaccoLedgers(22));
            getFacade().saveLedgerEntry(sle);
            sle = new SaccoLedgerEntries();
            sle.setCr(0);
            sle.setIsGeneral(true);
            sle.setParticulars("Loan Recovery");
            sle.setDateWhen(new Date());
            sle.setDr(depo.getAmount() * -1);
            sle.setLedgerId(new SaccoLedgers(8));
            getFacade().saveLedgerEntry(sle);
        }
        if (listg != null && listg.size() > 0) {
            int amountGuaranteed = getTotalForAllGuarantor();
            int amountToDistribute = (int) (loan.getAmount() - amountGuaranteed);
            ArrayList<Guarantors> withAmountNotSpecified = new ArrayList<>();
            for (Guarantors g : listg) {
                if (g.getGuaranteedAmount() == 0) {
                    withAmountNotSpecified.add(g);
                }
            }
            int forEachMember;
            try {
                forEachMember = (int) Math.ceil(amountToDistribute / withAmountNotSpecified.size());
            } catch (ArithmeticException e) {
                forEachMember = 0;
            }
            for (Guarantors g : listg) {
                AppConstants.logger("amount:" + g.getGuaranteedAmount());
                if (g.getGuarantorId() != null) {
                    if (g.getGuaranteedAmount() == 0) {
                        g.setGuaranteedAmount(forEachMember);
                    }
                    g.setId(null);
                    g.setLoanId(loan);
                    getFacade().storeGuarator(g);
                }
            }
        }
        selected = getFacade().findSelected(selected);
        JsfUtil.addSuccessMessage("Loan Created");
    }

    public void createGuarantor() {
        if (listg != null && listg.size() > 0) {
            int amountGuaranteed = getTotalForAllGuarantor();
            int amountToDistribute = (int) (loan.getAmount() - amountGuaranteed);
            ArrayList<Guarantors> withAmountNotSpecified = new ArrayList<>();
            for (Guarantors g : listg) {
                if (g.getGuaranteedAmount() == 0) {
                    withAmountNotSpecified.add(g);
                }
            }
            int forEachMember;
            try {
                forEachMember = (int) Math.ceil(amountToDistribute / withAmountNotSpecified.size());
            } catch (ArithmeticException e) {
                forEachMember = 0;
            }
            for (Guarantors g : listg) {
                AppConstants.logger("amount:" + g.getGuaranteedAmount());
                if (g.getGuarantorId() != null) {
                    if (g.getGuaranteedAmount() == 0) {
                        g.setGuaranteedAmount(forEachMember);
                    }
                    g.setLoanId(loan);
                    if (g.getId() == null) {
                        AppConstants.logger("stored");
                        getFacade().storeGuarator(g);
                    } else {
                        AppConstants.logger("Merged");
                        getFacade().mergeGuarantor(g);
                    }

                }
            }
        }
        selected = getFacade().findSelected(selected);
        loan = getFacade().findLoan(loan.getLoanId());
        FacesContext.getCurrentInstance().addMessage(
                null,
                new FacesMessage(FacesMessage.SEVERITY_INFO,
                        "Guarantors added", null));
    }

    public void payLoanFromDeposit() {
        if (loan != null) {
            if (depo.getAmount() > 0) {
                if (selected.getTotalDeposit() > depo.getAmount()) {
                    if (selected.getTotalDepositWithrawable() > 0) {
                        if (depo.getAmount() <= selected.getTotalDepositWithrawable()) {
                            Collections c = new Collections();
                            c.setAmount(depo.getAmount());
                            c.setDateCollected(new Date());
                            c.setMemberId(selected);
                            c.setRandomInt(-1);
                            c.setReceiptNo(1111);
                            int toDistribute = depo.getAmount();
                            if (loan.getLoantype().getId() == 1) {
                                c.setCollectionstype(new CollectionTypes(2));
                                int unpaidInterest = selected.getEstimatedUnPaidInterestEmergency();
                                int unpaidNegotiation = selected.getEstimatedUnPaidNegotiationEmergency();
                                if (unpaidInterest > 0) {
                                    if (toDistribute > unpaidInterest) {
                                        SaccoLedgerEntries sle = new SaccoLedgerEntries();
                                        sle.setParticulars("interest on loans");
                                        sle.setCr(unpaidInterest);
                                        sle.setDateWhen(new Date());
                                        sle.setDr(0);
                                        sle.setIsGeneral(false);
                                        sle.setStatusBilled(false);
                                        sle.setLedgerId(new SaccoLedgers(30));
                                        getFacade().saveLedgerEntry(sle);
                                        sle = new SaccoLedgerEntries();
                                        sle.setCr(0);
                                        sle.setIsGeneral(false);
                                        sle.setParticulars("interest on loans");
                                        sle.setDateWhen(new Date());
                                        sle.setStatusBilled(false);
                                        sle.setDr(unpaidInterest);
                                        sle.setLedgerId(new SaccoLedgers(8));
                                        getFacade().saveLedgerEntry(sle);
                                        toDistribute = toDistribute - unpaidInterest;
                                    } else {
                                        SaccoLedgerEntries sle = new SaccoLedgerEntries();
                                        sle.setParticulars("interest on loans");
                                        sle.setCr(toDistribute);
                                        sle.setDateWhen(new Date());
                                        sle.setDr(0);
                                        sle.setIsGeneral(false);
                                        sle.setStatusBilled(false);
                                        sle.setLedgerId(new SaccoLedgers(30));
                                        getFacade().saveLedgerEntry(sle);
                                        sle = new SaccoLedgerEntries();
                                        sle.setCr(0);
                                        sle.setParticulars("interest on loans");
                                        sle.setDateWhen(new Date());
                                        sle.setStatusBilled(false);
                                        sle.setIsGeneral(false);
                                        sle.setDr(toDistribute);
                                        sle.setLedgerId(new SaccoLedgers(8));
                                        getFacade().saveLedgerEntry(sle);
                                        toDistribute = 0;
                                    }

                                }
                                if (unpaidNegotiation > 0 && toDistribute > 0) {
                                    if (toDistribute > unpaidNegotiation) {
                                        SaccoLedgerEntries sle = new SaccoLedgerEntries();
                                        sle.setParticulars("loan negotiation");
                                        sle.setCr(unpaidInterest);
                                        sle.setDateWhen(new Date());
                                        sle.setDr(0);
                                        sle.setStatusBilled(false);
                                        sle.setLedgerId(new SaccoLedgers(41));
                                        getFacade().saveLedgerEntry(sle);
                                        sle = new SaccoLedgerEntries();
                                        sle.setCr(0);
                                        sle.setParticulars("loan negotiation");
                                        sle.setDateWhen(new Date());
                                        sle.setStatusBilled(false);
                                        sle.setDr(unpaidNegotiation);
                                        sle.setLedgerId(new SaccoLedgers(8));
                                        getFacade().saveLedgerEntry(sle);
                                        toDistribute = toDistribute - unpaidNegotiation;
                                    } else {
                                        SaccoLedgerEntries sle = new SaccoLedgerEntries();
                                        sle.setParticulars("loan negotiation");
                                        sle.setCr(toDistribute);
                                        sle.setDateWhen(new Date());
                                        sle.setDr(0);
                                        sle.setStatusBilled(false);
                                        sle.setLedgerId(new SaccoLedgers(41));
                                        getFacade().saveLedgerEntry(sle);
                                        sle = new SaccoLedgerEntries();
                                        sle.setCr(0);
                                        sle.setParticulars("loan negotiation");
                                        sle.setDateWhen(new Date());
                                        sle.setStatusBilled(false);
                                        sle.setDr(toDistribute);
                                        sle.setLedgerId(new SaccoLedgers(8));
                                        getFacade().saveLedgerEntry(sle);
                                        toDistribute = 0;
                                    }
                                }
                            } else if (loan.getLoantype().getId() == 2) {
                                c.setCollectionstype(new CollectionTypes(3));
                                int unpaidInterest = selected.getEstimatedUnPaidInterestNormal();
                                int unpaidNegotiation = selected.getEstimatedUnPaidNegotiationNormal();
                                if (unpaidInterest > 0) {
                                    if (toDistribute > unpaidInterest) {
                                        SaccoLedgerEntries sle = new SaccoLedgerEntries();
                                        sle.setParticulars("interest on loans");
                                        sle.setCr(unpaidInterest);
                                        sle.setDateWhen(new Date());
                                        sle.setDr(0);
                                        sle.setIsGeneral(false);
                                        sle.setStatusBilled(false);
                                        sle.setLedgerId(new SaccoLedgers(30));
                                        getFacade().saveLedgerEntry(sle);
                                        sle = new SaccoLedgerEntries();
                                        sle.setCr(0);
                                        sle.setParticulars("interest on loans");
                                        sle.setDateWhen(new Date());
                                        sle.setStatusBilled(false);
                                        sle.setIsGeneral(false);
                                        sle.setDr(unpaidInterest);
                                        sle.setLedgerId(new SaccoLedgers(8));
                                        getFacade().saveLedgerEntry(sle);
                                        toDistribute = toDistribute - unpaidInterest;
                                    } else {
                                        SaccoLedgerEntries sle = new SaccoLedgerEntries();
                                        sle.setParticulars("interest on loans");
                                        sle.setCr(toDistribute);
                                        sle.setDateWhen(new Date());
                                        sle.setDr(0);
                                        sle.setIsGeneral(false);
                                        sle.setStatusBilled(false);
                                        sle.setLedgerId(new SaccoLedgers(30));
                                        getFacade().saveLedgerEntry(sle);
                                        sle = new SaccoLedgerEntries();
                                        sle.setCr(0);
                                        sle.setParticulars("interest on loans");
                                        sle.setDateWhen(new Date());
                                        sle.setStatusBilled(false);
                                        sle.setDr(toDistribute);
                                        sle.setIsGeneral(false);
                                        sle.setLedgerId(new SaccoLedgers(8));
                                        getFacade().saveLedgerEntry(sle);
                                        toDistribute = 0;
                                    }

                                }
                                if (unpaidNegotiation > 0 && toDistribute > 0) {
                                    if (toDistribute > unpaidNegotiation) {
                                        SaccoLedgerEntries sle = new SaccoLedgerEntries();
                                        sle.setParticulars("loan negotiation");
                                        sle.setCr(unpaidInterest);
                                        sle.setDateWhen(new Date());
                                        sle.setDr(0);
                                        sle.setIsGeneral(false);
                                        sle.setStatusBilled(false);
                                        sle.setLedgerId(new SaccoLedgers(41));
                                        getFacade().saveLedgerEntry(sle);
                                        sle = new SaccoLedgerEntries();
                                        sle.setCr(0);
                                        sle.setIsGeneral(false);
                                        sle.setParticulars("loan negotiation");
                                        sle.setDateWhen(new Date());
                                        sle.setStatusBilled(false);
                                        sle.setDr(unpaidNegotiation);
                                        sle.setLedgerId(new SaccoLedgers(8));
                                        getFacade().saveLedgerEntry(sle);
                                        toDistribute = toDistribute - unpaidNegotiation;
                                    } else {
                                        SaccoLedgerEntries sle = new SaccoLedgerEntries();
                                        sle.setParticulars("interest on loans");
                                        sle.setCr(toDistribute);
                                        sle.setDateWhen(new Date());
                                        sle.setDr(0);
                                        sle.setIsGeneral(false);
                                        sle.setStatusBilled(false);
                                        sle.setLedgerId(new SaccoLedgers(41));
                                        getFacade().saveLedgerEntry(sle);
                                        sle = new SaccoLedgerEntries();
                                        sle.setCr(0);
                                        sle.setIsGeneral(false);
                                        sle.setParticulars("interest on loans");
                                        sle.setDateWhen(new Date());
                                        sle.setStatusBilled(false);
                                        sle.setDr(toDistribute);
                                        sle.setLedgerId(new SaccoLedgers(8));
                                        getFacade().saveLedgerEntry(sle);
                                        toDistribute = 0;
                                    }
                                }
                            }
                            //+++++
                            if (toDistribute > 0) {
                                SaccoLedgerEntries sle = new SaccoLedgerEntries();
                                sle.setCr(0);
                                sle.setIsGeneral(false);
                                sle.setDateWhen(new Date());
                                sle.setDr(toDistribute);
                                sle.setLedgerId(new SaccoLedgers(8));
                                sle.setParticulars("loan collections");
                                getFacade().saveLedgerEntry(sle);
                                sle = new SaccoLedgerEntries();
                                sle.setParticulars("loan collections");
                                sle.setCr(toDistribute);
                                sle.setDateWhen(new Date());
                                sle.setDr(0);
                                sle.setIsGeneral(false);
                                sle.setLedgerId(new SaccoLedgers(22));
                                getFacade().saveLedgerEntry(sle);
                            }
                            getFacade().storeCollections(c);
                            int toremain = depo.getAmount() - selected.getTotalDepositWithrawable();
                            depo.setAmount(selected.getTotalDepositWithrawable() * -1);
                            depo.setChequeNo("");
                            depo.setPaymentMode(new PaymentModes(1));
                            depo.setMemberNo(selected);
                            depo.setTransactedBy("");
                            depo.setDate(new Date());
                            depo.setDescription("paid loan");
                            depo.setIsWithdrawable(true);
                            getFacade().storeDeposit(depo);
                            depo = new DepositCash();
                            depo.setAmount(toremain * -1);
                            depo.setChequeNo("");
                            depo.setPaymentMode(new PaymentModes(1));
                            depo.setMemberNo(selected);
                            depo.setTransactedBy("");
                            depo.setDate(new Date());
                            depo.setDescription("paid loan");
                            depo.setIsWithdrawable(false);
                            getFacade().storeDeposit(depo);
                            depo = null;
                            loan = null;
                            selected = getFacade().findSelected(selected);
                            selected = getFacade().findSelected(selected);
                            FacesContext.getCurrentInstance().addMessage(
                                    null,
                                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                                            "loan paid", null));
                        } else {
                            Collections c = new Collections();
                            c.setAmount(depo.getAmount());
                            c.setDateCollected(new Date());
                            c.setMemberId(selected);
                            c.setRandomInt(-1);
                            c.setReceiptNo(1111);
                            int toDistribute = depo.getAmount();
                            if (loan.getLoantype().getId() == 1) {
                                c.setCollectionstype(new CollectionTypes(2));
                                int unpaidInterest = selected.getEstimatedUnPaidInterestEmergency();
                                int unpaidNegotiation = selected.getEstimatedUnPaidNegotiationEmergency();
                                if (unpaidInterest > 0) {
                                    if (toDistribute > unpaidInterest) {
                                        SaccoLedgerEntries sle = new SaccoLedgerEntries();
                                        sle.setParticulars("interest on loans");
                                        sle.setCr(unpaidInterest);
                                        sle.setDateWhen(new Date());
                                        sle.setDr(0);
                                        sle.setIsGeneral(false);
                                        sle.setStatusBilled(false);
                                        sle.setLedgerId(new SaccoLedgers(30));
                                        getFacade().saveLedgerEntry(sle);
                                        sle = new SaccoLedgerEntries();
                                        sle.setCr(0);
                                        sle.setIsGeneral(false);
                                        sle.setParticulars("interest on loans");
                                        sle.setDateWhen(new Date());
                                        sle.setStatusBilled(false);
                                        sle.setDr(unpaidInterest);
                                        sle.setLedgerId(new SaccoLedgers(8));
                                        getFacade().saveLedgerEntry(sle);
                                        toDistribute = toDistribute - unpaidInterest;
                                    } else {
                                        SaccoLedgerEntries sle = new SaccoLedgerEntries();
                                        sle.setParticulars("interest on loans");
                                        sle.setCr(toDistribute);
                                        sle.setDateWhen(new Date());
                                        sle.setDr(0);
                                        sle.setIsGeneral(false);
                                        sle.setStatusBilled(false);
                                        sle.setLedgerId(new SaccoLedgers(30));
                                        getFacade().saveLedgerEntry(sle);
                                        sle = new SaccoLedgerEntries();
                                        sle.setCr(0);
                                        sle.setParticulars("interest on loans");
                                        sle.setDateWhen(new Date());
                                        sle.setStatusBilled(false);
                                        sle.setIsGeneral(false);
                                        sle.setDr(toDistribute);
                                        sle.setLedgerId(new SaccoLedgers(8));
                                        getFacade().saveLedgerEntry(sle);
                                        toDistribute = 0;
                                    }

                                }
                                if (unpaidNegotiation > 0 && toDistribute > 0) {
                                    if (toDistribute > unpaidNegotiation) {
                                        SaccoLedgerEntries sle = new SaccoLedgerEntries();
                                        sle.setParticulars("loan negotiation");
                                        sle.setCr(unpaidInterest);
                                        sle.setDateWhen(new Date());
                                        sle.setDr(0);
                                        sle.setStatusBilled(false);
                                        sle.setLedgerId(new SaccoLedgers(41));
                                        getFacade().saveLedgerEntry(sle);
                                        sle = new SaccoLedgerEntries();
                                        sle.setCr(0);
                                        sle.setParticulars("loan negotiation");
                                        sle.setDateWhen(new Date());
                                        sle.setStatusBilled(false);
                                        sle.setDr(unpaidNegotiation);
                                        sle.setLedgerId(new SaccoLedgers(8));
                                        getFacade().saveLedgerEntry(sle);
                                        toDistribute = toDistribute - unpaidNegotiation;
                                    } else {
                                        SaccoLedgerEntries sle = new SaccoLedgerEntries();
                                        sle.setParticulars("loan negotiation");
                                        sle.setCr(toDistribute);
                                        sle.setDateWhen(new Date());
                                        sle.setDr(0);
                                        sle.setStatusBilled(false);
                                        sle.setLedgerId(new SaccoLedgers(41));
                                        getFacade().saveLedgerEntry(sle);
                                        sle = new SaccoLedgerEntries();
                                        sle.setCr(0);
                                        sle.setParticulars("loan negotiation");
                                        sle.setDateWhen(new Date());
                                        sle.setStatusBilled(false);
                                        sle.setDr(toDistribute);
                                        sle.setLedgerId(new SaccoLedgers(8));
                                        getFacade().saveLedgerEntry(sle);
                                        toDistribute = 0;
                                    }
                                }
                            } else if (loan.getLoantype().getId() == 2) {
                                c.setCollectionstype(new CollectionTypes(3));
                                int unpaidInterest = selected.getEstimatedUnPaidInterestNormal();
                                int unpaidNegotiation = selected.getEstimatedUnPaidNegotiationNormal();
                                if (unpaidInterest > 0) {
                                    if (toDistribute > unpaidInterest) {
                                        SaccoLedgerEntries sle = new SaccoLedgerEntries();
                                        sle.setParticulars("interest on loans");
                                        sle.setCr(unpaidInterest);
                                        sle.setDateWhen(new Date());
                                        sle.setDr(0);
                                        sle.setIsGeneral(false);
                                        sle.setStatusBilled(false);
                                        sle.setLedgerId(new SaccoLedgers(30));
                                        getFacade().saveLedgerEntry(sle);
                                        sle = new SaccoLedgerEntries();
                                        sle.setCr(0);
                                        sle.setParticulars("interest on loans");
                                        sle.setDateWhen(new Date());
                                        sle.setStatusBilled(false);
                                        sle.setIsGeneral(false);
                                        sle.setDr(unpaidInterest);
                                        sle.setLedgerId(new SaccoLedgers(8));
                                        getFacade().saveLedgerEntry(sle);
                                        toDistribute = toDistribute - unpaidInterest;
                                    } else {
                                        SaccoLedgerEntries sle = new SaccoLedgerEntries();
                                        sle.setParticulars("interest on loans");
                                        sle.setCr(toDistribute);
                                        sle.setDateWhen(new Date());
                                        sle.setDr(0);
                                        sle.setIsGeneral(false);
                                        sle.setStatusBilled(false);
                                        sle.setLedgerId(new SaccoLedgers(30));
                                        getFacade().saveLedgerEntry(sle);
                                        sle = new SaccoLedgerEntries();
                                        sle.setCr(0);
                                        sle.setParticulars("interest on loans");
                                        sle.setDateWhen(new Date());
                                        sle.setStatusBilled(false);
                                        sle.setDr(toDistribute);
                                        sle.setIsGeneral(false);
                                        sle.setLedgerId(new SaccoLedgers(8));
                                        getFacade().saveLedgerEntry(sle);
                                        toDistribute = 0;
                                    }

                                }
                                if (unpaidNegotiation > 0 && toDistribute > 0) {
                                    if (toDistribute > unpaidNegotiation) {
                                        SaccoLedgerEntries sle = new SaccoLedgerEntries();
                                        sle.setParticulars("loan negotiation");
                                        sle.setCr(unpaidInterest);
                                        sle.setDateWhen(new Date());
                                        sle.setDr(0);
                                        sle.setIsGeneral(false);
                                        sle.setStatusBilled(false);
                                        sle.setLedgerId(new SaccoLedgers(41));
                                        getFacade().saveLedgerEntry(sle);
                                        sle = new SaccoLedgerEntries();
                                        sle.setCr(0);
                                        sle.setIsGeneral(false);
                                        sle.setParticulars("loan negotiation");
                                        sle.setDateWhen(new Date());
                                        sle.setStatusBilled(false);
                                        sle.setDr(unpaidNegotiation);
                                        sle.setLedgerId(new SaccoLedgers(8));
                                        getFacade().saveLedgerEntry(sle);
                                        toDistribute = toDistribute - unpaidNegotiation;
                                    } else {
                                        SaccoLedgerEntries sle = new SaccoLedgerEntries();
                                        sle.setParticulars("interest on loans");
                                        sle.setCr(toDistribute);
                                        sle.setDateWhen(new Date());
                                        sle.setDr(0);
                                        sle.setIsGeneral(false);
                                        sle.setStatusBilled(false);
                                        sle.setLedgerId(new SaccoLedgers(41));
                                        getFacade().saveLedgerEntry(sle);
                                        sle = new SaccoLedgerEntries();
                                        sle.setCr(0);
                                        sle.setIsGeneral(false);
                                        sle.setParticulars("interest on loans");
                                        sle.setDateWhen(new Date());
                                        sle.setStatusBilled(false);
                                        sle.setDr(toDistribute);
                                        sle.setLedgerId(new SaccoLedgers(8));
                                        getFacade().saveLedgerEntry(sle);
                                        toDistribute = 0;
                                    }
                                }
                            }
                            //+++++
                            if (toDistribute > 0) {
                                SaccoLedgerEntries sle = new SaccoLedgerEntries();
                                sle.setCr(0);
                                sle.setIsGeneral(false);
                                sle.setDateWhen(new Date());
                                sle.setDr(toDistribute);
                                sle.setLedgerId(new SaccoLedgers(8));
                                sle.setParticulars("loan collections");
                                getFacade().saveLedgerEntry(sle);
                                sle = new SaccoLedgerEntries();
                                sle.setParticulars("loan collections");
                                sle.setCr(toDistribute);
                                sle.setDateWhen(new Date());
                                sle.setDr(0);
                                sle.setIsGeneral(false);
                                sle.setLedgerId(new SaccoLedgers(22));
                                getFacade().saveLedgerEntry(sle);
                            }
                            getFacade().storeCollections(c);
                            depo.setAmount(depo.getAmount() * -1);
                            depo.setChequeNo("");
                            depo.setPaymentMode(new PaymentModes(1));
                            depo.setMemberNo(selected);
                            depo.setTransactedBy("");
                            depo.setDate(new Date());
                            depo.setDescription("paid loan");
                            depo.setIsWithdrawable(true);
                            getFacade().storeDeposit(depo);
                            depo = null;
                            loan = null;
                            selected = getFacade().findSelected(selected);
                            selected = getFacade().findSelected(selected);
                            FacesContext.getCurrentInstance().addMessage(
                                    null,
                                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                                            "loan paid", null));
                        }
                    } else if (selected.getTotalDepositNonWithrawable() >= depo.getAmount()) {
                        Collections c = new Collections();
                        c.setAmount(depo.getAmount());
                        c.setDateCollected(new Date());
                        c.setMemberId(selected);
                        c.setRandomInt(-1);
                        c.setReceiptNo(1111);
                        int toDistribute = depo.getAmount();
                        if (loan.getLoantype().getId() == 1) {
                            c.setCollectionstype(new CollectionTypes(2));
                            int unpaidInterest = selected.getEstimatedUnPaidInterestEmergency();
                            int unpaidNegotiation = selected.getEstimatedUnPaidNegotiationEmergency();
                            if (unpaidInterest > 0) {
                                if (toDistribute > unpaidInterest) {
                                    SaccoLedgerEntries sle = new SaccoLedgerEntries();
                                    sle.setParticulars("interest on loans");
                                    sle.setCr(unpaidInterest);
                                    sle.setDateWhen(new Date());
                                    sle.setDr(0);
                                    sle.setIsGeneral(false);
                                    sle.setStatusBilled(false);
                                    sle.setLedgerId(new SaccoLedgers(30));
                                    getFacade().saveLedgerEntry(sle);
                                    sle = new SaccoLedgerEntries();
                                    sle.setCr(0);
                                    sle.setIsGeneral(false);
                                    sle.setParticulars("interest on loans");
                                    sle.setDateWhen(new Date());
                                    sle.setStatusBilled(false);
                                    sle.setDr(unpaidInterest);
                                    sle.setLedgerId(new SaccoLedgers(8));
                                    getFacade().saveLedgerEntry(sle);
                                    toDistribute = toDistribute - unpaidInterest;
                                } else {
                                    SaccoLedgerEntries sle = new SaccoLedgerEntries();
                                    sle.setParticulars("interest on loans");
                                    sle.setCr(toDistribute);
                                    sle.setDateWhen(new Date());
                                    sle.setDr(0);
                                    sle.setIsGeneral(false);
                                    sle.setStatusBilled(false);
                                    sle.setLedgerId(new SaccoLedgers(30));
                                    getFacade().saveLedgerEntry(sle);
                                    sle = new SaccoLedgerEntries();
                                    sle.setCr(0);
                                    sle.setParticulars("interest on loans");
                                    sle.setDateWhen(new Date());
                                    sle.setStatusBilled(false);
                                    sle.setIsGeneral(false);
                                    sle.setDr(toDistribute);
                                    sle.setLedgerId(new SaccoLedgers(8));
                                    getFacade().saveLedgerEntry(sle);
                                    toDistribute = 0;
                                }

                            }
                            if (unpaidNegotiation > 0 && toDistribute > 0) {
                                if (toDistribute > unpaidNegotiation) {
                                    SaccoLedgerEntries sle = new SaccoLedgerEntries();
                                    sle.setParticulars("loan negotiation");
                                    sle.setCr(unpaidInterest);
                                    sle.setDateWhen(new Date());
                                    sle.setDr(0);
                                    sle.setStatusBilled(false);
                                    sle.setLedgerId(new SaccoLedgers(41));
                                    getFacade().saveLedgerEntry(sle);
                                    sle = new SaccoLedgerEntries();
                                    sle.setCr(0);
                                    sle.setParticulars("loan negotiation");
                                    sle.setDateWhen(new Date());
                                    sle.setStatusBilled(false);
                                    sle.setDr(unpaidNegotiation);
                                    sle.setLedgerId(new SaccoLedgers(8));
                                    getFacade().saveLedgerEntry(sle);
                                    toDistribute = toDistribute - unpaidNegotiation;
                                } else {
                                    SaccoLedgerEntries sle = new SaccoLedgerEntries();
                                    sle.setParticulars("loan negotiation");
                                    sle.setCr(toDistribute);
                                    sle.setDateWhen(new Date());
                                    sle.setDr(0);
                                    sle.setStatusBilled(false);
                                    sle.setLedgerId(new SaccoLedgers(41));
                                    getFacade().saveLedgerEntry(sle);
                                    sle = new SaccoLedgerEntries();
                                    sle.setCr(0);
                                    sle.setParticulars("loan negotiation");
                                    sle.setDateWhen(new Date());
                                    sle.setStatusBilled(false);
                                    sle.setDr(toDistribute);
                                    sle.setLedgerId(new SaccoLedgers(8));
                                    getFacade().saveLedgerEntry(sle);
                                    toDistribute = 0;
                                }
                            }
                        } else if (loan.getLoantype().getId() == 2) {
                            c.setCollectionstype(new CollectionTypes(3));
                            int unpaidInterest = selected.getEstimatedUnPaidInterestNormal();
                            int unpaidNegotiation = selected.getEstimatedUnPaidNegotiationNormal();
                            if (unpaidInterest > 0) {
                                if (toDistribute > unpaidInterest) {
                                    SaccoLedgerEntries sle = new SaccoLedgerEntries();
                                    sle.setParticulars("interest on loans");
                                    sle.setCr(unpaidInterest);
                                    sle.setDateWhen(new Date());
                                    sle.setDr(0);
                                    sle.setIsGeneral(false);
                                    sle.setStatusBilled(false);
                                    sle.setLedgerId(new SaccoLedgers(30));
                                    getFacade().saveLedgerEntry(sle);
                                    sle = new SaccoLedgerEntries();
                                    sle.setCr(0);
                                    sle.setParticulars("interest on loans");
                                    sle.setDateWhen(new Date());
                                    sle.setStatusBilled(false);
                                    sle.setIsGeneral(false);
                                    sle.setDr(unpaidInterest);
                                    sle.setLedgerId(new SaccoLedgers(8));
                                    getFacade().saveLedgerEntry(sle);
                                    toDistribute = toDistribute - unpaidInterest;
                                } else {
                                    SaccoLedgerEntries sle = new SaccoLedgerEntries();
                                    sle.setParticulars("interest on loans");
                                    sle.setCr(toDistribute);
                                    sle.setDateWhen(new Date());
                                    sle.setDr(0);
                                    sle.setIsGeneral(false);
                                    sle.setStatusBilled(false);
                                    sle.setLedgerId(new SaccoLedgers(30));
                                    getFacade().saveLedgerEntry(sle);
                                    sle = new SaccoLedgerEntries();
                                    sle.setCr(0);
                                    sle.setParticulars("interest on loans");
                                    sle.setDateWhen(new Date());
                                    sle.setStatusBilled(false);
                                    sle.setDr(toDistribute);
                                    sle.setIsGeneral(false);
                                    sle.setLedgerId(new SaccoLedgers(8));
                                    getFacade().saveLedgerEntry(sle);
                                    toDistribute = 0;
                                }

                            }
                            if (unpaidNegotiation > 0 && toDistribute > 0) {
                                if (toDistribute > unpaidNegotiation) {
                                    SaccoLedgerEntries sle = new SaccoLedgerEntries();
                                    sle.setParticulars("loan negotiation");
                                    sle.setCr(unpaidInterest);
                                    sle.setDateWhen(new Date());
                                    sle.setDr(0);
                                    sle.setIsGeneral(false);
                                    sle.setStatusBilled(false);
                                    sle.setLedgerId(new SaccoLedgers(41));
                                    getFacade().saveLedgerEntry(sle);
                                    sle = new SaccoLedgerEntries();
                                    sle.setCr(0);
                                    sle.setIsGeneral(false);
                                    sle.setParticulars("loan negotiation");
                                    sle.setDateWhen(new Date());
                                    sle.setStatusBilled(false);
                                    sle.setDr(unpaidNegotiation);
                                    sle.setLedgerId(new SaccoLedgers(8));
                                    getFacade().saveLedgerEntry(sle);
                                    toDistribute = toDistribute - unpaidNegotiation;
                                } else {
                                    SaccoLedgerEntries sle = new SaccoLedgerEntries();
                                    sle.setParticulars("interest on loans");
                                    sle.setCr(toDistribute);
                                    sle.setDateWhen(new Date());
                                    sle.setDr(0);
                                    sle.setIsGeneral(false);
                                    sle.setStatusBilled(false);
                                    sle.setLedgerId(new SaccoLedgers(41));
                                    getFacade().saveLedgerEntry(sle);
                                    sle = new SaccoLedgerEntries();
                                    sle.setCr(0);
                                    sle.setIsGeneral(false);
                                    sle.setParticulars("interest on loans");
                                    sle.setDateWhen(new Date());
                                    sle.setStatusBilled(false);
                                    sle.setDr(toDistribute);
                                    sle.setLedgerId(new SaccoLedgers(8));
                                    getFacade().saveLedgerEntry(sle);
                                    toDistribute = 0;
                                }
                            }
                        }
                        //+++++
                        if (toDistribute > 0) {
                            SaccoLedgerEntries sle = new SaccoLedgerEntries();
                            sle.setCr(0);
                            sle.setIsGeneral(false);
                            sle.setDateWhen(new Date());
                            sle.setDr(toDistribute);
                            sle.setLedgerId(new SaccoLedgers(8));
                            sle.setParticulars("loan collections");
                            getFacade().saveLedgerEntry(sle);
                            sle = new SaccoLedgerEntries();
                            sle.setParticulars("loan collections");
                            sle.setCr(toDistribute);
                            sle.setDateWhen(new Date());
                            sle.setDr(0);
                            sle.setIsGeneral(false);
                            sle.setLedgerId(new SaccoLedgers(22));
                            getFacade().saveLedgerEntry(sle);
                        }
                        getFacade().storeCollections(c);
                        depo.setAmount(depo.getAmount() * -1);
                        depo.setChequeNo("");
                        depo.setPaymentMode(new PaymentModes(1));
                        depo.setMemberNo(selected);
                        depo.setTransactedBy("");
                        depo.setDate(new Date());
                        depo.setDescription("paid loan");
                        depo.setIsWithdrawable(false);
                        getFacade().storeDeposit(depo);
                        depo = null;
                        loan = null;
                        selected = getFacade().findSelected(selected);
                        selected = getFacade().findSelected(selected);
                        FacesContext.getCurrentInstance().addMessage(
                                null,
                                new FacesMessage(FacesMessage.SEVERITY_INFO,
                                        "loan paid", null));
                    }
                } else {
                    FacesContext.getCurrentInstance().addMessage(
                            null,
                            new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                    "Loan not paid. Limited account balance", null));
                }
            }
        }
    }

    public void deductFromGuarantors() {
        int noOfGuarantor = loan.getGuarantorsCollection().size();
        AppConstants.logger("guarantors" + noOfGuarantor);
        int guaranteedAmount = loan.getTotalGuaranteedAmount();
    }

    public void onRowEdit(RowEditEvent event) {
        Guarantors newg = (Guarantors) event.getObject();
        listg.get(newg.getId()).setGuaranteedAmount(newg.getGuaranteedAmount());
        listg.get(newg.getId()).setGuarantorId(newg.getGuarantorId());
    }

    public void onRowCancel(RowEditEvent event) {
        AppConstants.logger("yeiuyuieyiru");
        FacesContext.getCurrentInstance().addMessage(
                null,
                new FacesMessage(FacesMessage.SEVERITY_FATAL,
                        "No record have been saved", null));
    }

    public void setNoOfGuarantors(int noOfGuarantors) {
        this.noOfGuarantors = noOfGuarantors;
    }

    public LoanScheduleTransactions getItemTransaction() {
        return itemtransaction;
    }

    public DepositCash getMoneyTransfer() {
        return moneyTransfer;
    }

    public void setMoneyTransfer(DepositCash moneyTransfer) {
        this.moneyTransfer = moneyTransfer;
    }

    public void saveDeposit() {
        if (moneyTransfer.getAmount() <= selected.getTotalDepositWithrawable()) {
            getFacade().storeDeposit(moneyTransfer);
            moneyTransfer.setAmount(moneyTransfer.getAmount() * -1);
            moneyTransfer.setDate(new Date());
            moneyTransfer.setChequeNo("1111");
            moneyTransfer.setDescription("Tranfered");
            moneyTransfer.setIsWithdrawable(true);
            moneyTransfer.setMemberNo(selected);
            moneyTransfer.setPaymentMode(new PaymentModes(1));
            moneyTransfer.setTransactedBy("jack");
            getFacade().storeDeposit(moneyTransfer);
            selected = getFacade().findSelected(selected);
            FacesContext.getCurrentInstance().addMessage(
                    null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Transfer successiful", null));
            return;
        }
        FacesContext.getCurrentInstance().addMessage(
                null,
                new FacesMessage(FacesMessage.SEVERITY_FATAL,
                        "Transfer failed, Insufficient funds", null));
    }
    private DepositCash moneyTransfer;

    public void prepareCreatedeposit() {
        moneyTransfer = new DepositCash();
        moneyTransfer.setAmount(selected.getTotalDepositWithrawable());
        moneyTransfer.setDate(new Date());
        moneyTransfer.setChequeNo("1111");
        moneyTransfer.setDescription("Tranfered");
        moneyTransfer.setIsWithdrawable(false);
        moneyTransfer.setMemberNo(selected);
        moneyTransfer.setPaymentMode(new PaymentModes(1));
        moneyTransfer.setTransactedBy("jack");
    }

    public List<Collections> getCollectionsBetweenDates() {
        return getFacade().getCollectionsBetweenDates(selected, sdate, edate, type);
    }

    public List<Collections> getCollectionsBetweenDatesAll() {
        return getFacade().getCollectionsBetweenDates(selected, sdate, edate);
    }

    public int getTotalCollectionsBetweenDates() {
        int total = 0;
        for (Collections c : getCollectionsBetweenDates()) {
            total += c.getAmount();
        }
        return total;
    }

    public int getTotalCollectionsBetweenDatesAll() {
        int total = 0;
        for (Collections c : getCollectionsBetweenDatesAll()) {
            total += c.getAmount();
        }
        return total;
    }

    public void setItemTransaction(LoanScheduleTransactions itemtransaction) {
        this.itemtransaction = itemtransaction;
    }

    public Loan getLoan() {
        return loan;
    }

    public Date getSdate() {
        return sdate;
    }

    public void setSdate(Date sdate) {
        this.sdate = sdate;
    }

    public Date getEdate() {
        return edate;
    }

    public void setEdate(Date edate) {
        this.edate = edate;
    }

    public void setLoan(Loan loan) {
        this.loan = loan;
    }

    private PaymentSummaryItems item;

    public void closeAllAcoounts() {
        ///some code missing here
    }
    public void onSelect() {
        loan = null;
    }

    public void payLoan(int amount, int type, Loan loan) {
        ///some code missing here
    }

    public void closeSchedule(int l, Loan loan) {
        ///some code missing here
    }

    public void createDepo(int amount, String desc, Members id) {
        depo = new DepositCash();
        depo.setAmount(amount);
        depo.setChequeNo("");
        depo.setDescription(desc);
        depo.setPaymentMode(new PaymentModes(1));
        depo.setMemberNo(id);
        depo.setTransactedBy("");
        depo.setDate(new Date());
        getFacade().storeDeposit(depo);
    }

    public int getTotalForAllGuarantor() {
        int total = 0;
        if (getGuarators() != null) {
            for (Guarantors g : getGuarators()) {
                total += g.getGuaranteedAmount();
            }
        }
        return total;
    }

    public void updateMember() {
        selected = getFacade().findSelected(selected);
        try {
            loan = getFacade().findLoan(loan.getLoanId());
        } catch (NullPointerException e) {

        }
    }

    public void performWithDrawal() {
        ///some code missing here
    }

    public int getWithdrawalcharges() {
        return withdrawalcharges;
    }

    public void setWithdrawalcharges(int withdrawalcharges) {
        this.withdrawalcharges = withdrawalcharges;
    }

    public String getConfirmationMessage() {
        return "\nLast updated " + lastUpdated.toString();
    }

    public void createDeposit(int amount, Members id, int collectiontype) {
        AppConstants.logger("creating deposit for type" + collectiontype);
        SaccoLedgerEntries sle;
        AppConstants.logger("finding bills with arrears");
        if (id.getBillsTotalArrears() < 0) {
            AppConstants.logger("found bills with arrears");
            for (MemberBills mb : id.getMemberBillsCollection()) {
                if (amount > 0) {
                    if (mb.getNetAcc() < 0) {
                        if (amount == mb.getNetAcc() * -1 || amount < mb.getNetAcc() * -1) {
                            BillTransactions bt = new BillTransactions();
                            bt.setAmountCredit(amount);
                            bt.setDatePaid(new Date());
                            bt.setBillId(mb);
                            bt.setNaration(bt.getNaration());
                            bt.setReceiptNo("");
                            getFacade().storeBillTransaction(bt);
                            switch (bt.getNaration()) {
                                case "shares": {
                                    AppConstants.logger("setting cr for shares" + amount);
                                    sle = new SaccoLedgerEntries();
                                    sle.setCr(amount);
                                    sle.setDateWhen(new Date());
                                    sle.setDr(0);
                                    sle.setLedgerId(new SaccoLedgers(1));
                                    getFacade().saveLedgerEntry(sle);
                                    sle = new SaccoLedgerEntries();
                                    sle.setCr(0);
                                    sle.setDateWhen(new Date());
                                    sle.setDr(amount);
                                    sle.setLedgerId(new SaccoLedgers(22));
                                    getFacade().saveLedgerEntry(sle);
                                    break;
                                }
                                case "registration": {
                                    AppConstants.logger("setting cr for registration" + amount);
                                    sle = new SaccoLedgerEntries();
                                    sle.setCr(0);
                                    sle.setDateWhen(new Date());
                                    sle.setDr(amount);
                                    sle.setLedgerId(new SaccoLedgers(12));
                                    getFacade().saveLedgerEntry(sle);
                                    sle = new SaccoLedgerEntries();
                                    sle.setCr(0);
                                    sle.setDateWhen(new Date());
                                    sle.setDr(amount);
                                    sle.setLedgerId(new SaccoLedgers(22));
                                    getFacade().saveLedgerEntry(sle);
                                    break;
                                }
                            }
                            amount = 0;
                            break;
                        } else {
                            amount = amount - mb.getNetAcc() * -1;
                            BillTransactions bt = new BillTransactions();
                            bt.setAmountCredit(mb.getNetAcc() * -1);
                            bt.setDatePaid(new Date());
                            bt.setBillId(mb);
                            bt.setNaration(bt.getNaration());
                            bt.setReceiptNo("");
                            getFacade().storeBillTransaction(bt);
                            switch (bt.getNaration()) {
                                case "shares": {
                                    AppConstants.logger("setting cr for shares" + mb.getNetAcc() * -1);
                                    sle = new SaccoLedgerEntries();
                                    sle.setCr(mb.getNetAcc() * -1);
                                    sle.setDateWhen(new Date());
                                    sle.setDr(0);
                                    sle.setLedgerId(new SaccoLedgers(1));
                                    getFacade().saveLedgerEntry(sle);
                                    sle = new SaccoLedgerEntries();
                                    sle.setCr(0);
                                    sle.setDateWhen(new Date());
                                    sle.setDr(mb.getNetAcc() * -1);
                                    sle.setLedgerId(new SaccoLedgers(22));
                                    getFacade().saveLedgerEntry(sle);
                                    break;
                                }
                                case "registration": {
                                    AppConstants.logger("setting cr for registration" + mb.getNetAcc() * -1);
                                    sle = new SaccoLedgerEntries();
                                    sle.setCr(0);
                                    sle.setDateWhen(new Date());
                                    sle.setDr(mb.getNetAcc() * -1);
                                    sle.setLedgerId(new SaccoLedgers(12));
                                    getFacade().saveLedgerEntry(sle);
                                    sle = new SaccoLedgerEntries();
                                    sle.setCr(0);
                                    sle.setDateWhen(new Date());
                                    sle.setDr(mb.getNetAcc() * -1);
                                    sle.setLedgerId(new SaccoLedgers(22));
                                    getFacade().saveLedgerEntry(sle);
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }

        AppConstants.logger("after paying bills arrears, amount remaining is" + amount);
        if (amount > 0) {
            AppConstants.logger("Depositing cash");
            DepositCash deposit = new DepositCash();
            deposit.setAmount(amount);
            deposit.setAmount(amount);
            deposit.setMemberNo(id);
            getFacade().storeDeposit(deposit);
            AppConstants.logger("Depositing cash done");
            AppConstants.logger("Updating legders");
            sle = new SaccoLedgerEntries();
            sle.setCr(amount);
            sle.setDateWhen(new Date());
            sle.setDr(0);
            sle.setLedgerId(new SaccoLedgers(8));
            getFacade().saveLedgerEntry(sle);
            sle = new SaccoLedgerEntries();
            sle.setCr(0);
            sle.setDateWhen(new Date());
            sle.setDr(amount);
            sle.setLedgerId(new SaccoLedgers(22));
            getFacade().saveLedgerEntry(sle);
            AppConstants.logger("Updating legders done");
        }
        AppConstants.logger("Updating collections to billed");
        for (Collections c : getFacade().getMemberUnbilledCollections(id)) {
            if (c.getCollectionstype().getId() == collectiontype) {
                c.setStatusBilled(true);
                getFacade().mergeCollection(c);
            }
        }
    }

    public Loan prepareCreateLoan() {
        loan = new Loan();
        loan.setMemeberId(selected);
        loan.setLoanStatus(false);
        listg = new ArrayList<>();
        return loan;
    }

    public MembersController() {
    }

    public Members getSelected() {
        return selected;
    }

    public void setSelected(Members selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    public void updateAllAccountStatus() {
        for (Members m : getItems()) {
            AccountTransitions accTran = new AccountTransitions();
            accTran.setAccount(m);
            accTran.setComments("account activated");
            accTran.setDateWhen(new Date());
            accTran.setStatus(new AccountStatus(1));
            accTran.setIsCurrent(true);
            getFacade().StoreAccTransition(accTran);
        }
    }

    protected void initializeEmbeddableKey() {
    }

    public DepositCash getDepo() {
        return depo;
    }

    public void setDepo(DepositCash depo) {
        this.depo = depo;
    }

    private MembersFacade getFacade() {
        return ejbFacade;
    }

    public Members prepareCreate() {
        selected = new Members();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {
        int randomInt = new Random().nextInt(100000);
        selected.setRandomInt(randomInt);
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("MembersCreated"));
        if (!JsfUtil.isValidationFailed()) {
            selected = getFacade().getByRandomInt(randomInt);
            if (registrationfee > 0) {
                int randomInt2 = new Random().nextInt(100000);
                MemberBills mb = new MemberBills();
                mb.setRandomInt(randomInt2);
                mb.setAmount(registrationfee);
                mb.setDateIncured(new Date());
                mb.setMemberId(selected);
                mb.setNarration("registration");
                getFacade().persistBill(mb);
                mb = getFacade().getMemberBillByRandomInt(randomInt2);
                BillTransactions bt = new BillTransactions();
                bt.setBillId(mb);
                bt.setAmountDebit(-registrationfee);
                bt.setDatePaid(new Date());
                bt.setNaration("registration");
                bt.setReceiptNo("");
                bt.setAmountCredit(0);
                getFacade().storeBillTransaction(bt);
                mb.setRandomInt(-1);
                getFacade().mergeMemberBill(mb);
            }
            if (entryfee > 0) {
                int randomInt2 = new Random().nextInt(100000);
                MemberBills mb = new MemberBills();
                mb.setRandomInt(randomInt2);
                mb.setAmount(entryfee);
                mb.setDateIncured(new Date());
                mb.setMemberId(selected);
                mb.setNarration("shares");
                getFacade().persistBill(mb);
                mb = getFacade().getMemberBillByRandomInt(randomInt2);
                BillTransactions bt = new BillTransactions();
                bt.setBillId(mb);
                bt.setAmountDebit(-entryfee);
                bt.setDatePaid(new Date());
                bt.setNaration("shares");
                bt.setReceiptNo("");
                bt.setAmountCredit(0);
                getFacade().storeBillTransaction(bt);
                mb.setRandomInt(-1);
                getFacade().mergeMemberBill(mb);
            }
            AccountTransitions accTran = new AccountTransitions();
            accTran.setAccount(selected);
            accTran.setComments("account activated");
            accTran.setDateWhen(new Date());
            accTran.setStatus(new AccountStatus(1));
            accTran.setIsCurrent(true);
            getFacade().StoreAccTransition(accTran);
            selected.setRandomInt(-1);
            getFacade().edit(selected);
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("MembersUpdated"));
    }

    public DepositCash prepareDeposit() {
        depo = new DepositCash();
        depo.setMemberNo(selected);
        return depo;
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("MembersDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<LoanTypes> getLoanTypes() {
        return getFacade().getLoanTypes();
    }

    public List<Members> getItems() {
        items = getFacade().getAllMembers();
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }

    public Members getMembers(java.lang.Integer id) {
        return getFacade().find(id);
    }

    public List<Members> getItemsAvailableSelectMany() {
        return getFacade().getAllMembers();
    }

    public List<Members> getItemsAvailableSelectOne() {
        return getFacade().getAllMembers();
    }

    @FacesConverter(forClass = Members.class)
    public static class MembersControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            MembersController controller = (MembersController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "membersController");
            return controller.getMembers(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Members) {
                Members o = (Members) object;
                return getStringKey(o.getId());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Members.class.getName()});
                return null;
            }
        }
    }

    public String getTitle() {
        return "Sacco Members Payments Summary For The Month Of " + new SimpleDateFormat("MMMM-yyyy").format(new DateTime().minusMonths(1).toDate()).replace("-", " Year ");
    }

    public String getTitleMember() {
        try {
            return "Acc Statement for " + selected.getSurname() + " As at the Month Of " + new SimpleDateFormat("MMMM-yyyy").format(new DateTime().minusMonths(1).toDate()).replace("-", " Year ");
        } catch (NullPointerException e) {
        }
        return "Acc Statement";
    }

    public void store() {
        getFacade().storePaymenySummary(p);
    }

    public int getTotalShares() {
        int total = 0;
        if (getItems() != null) {
            for (Members m : getItems()) {
                total += m.getSharesPaid();
            }
        }
        return total;
    }

    public int getTotalReg() {
        int total = 0;
        if (getItems() != null) {
            for (Members m : getItems()) {
                total += m.getRegPaid();
            }
        }
        return total;
    }

    public int getTotalLsf() {
        int total = 0;
        if (getItems() != null) {
            for (Members m : getItems()) {
                total += m.getLoanDifferenceWithDeposits() - withdrawalcharges - m.getTotalRecoverableFromLoan();
            }
        }
        return total;
    }

    public int getTotalDepost() {
        int total = 0;
        if (getItems() != null) {
            for (Members m : getItems()) {
                total += m.getTotalDeposit();
            }
        }
        return total;
    }

    public int getTotalLoans() {
        int total = 0;
        if (getItems() != null) {
            for (Members m : getItems()) {
                total += m.getTotalUnpaidLoan();
            }
        }
        return total;
    }

    public int getTotalInterestEarned() {
        int total = 0;
        if (getItems() != null) {
            for (Members m : getItems()) {
                total += m.getTotalInterestPaidOnLoans();
            }
        }
        return total;
    }

    public int getTotalLoanRecovery() {
        int total = 0;
        if (getItems() != null) {
            for (Members m : getItems()) {
                total += m.getTotalLoanRecovery();
            }
        }
        return total;
    }

    public int getTotalWithDrawal() {
        int total = 0;
        if (getItems() != null) {
            for (Members m : getItems()) {
                total += m.getTotalWithdrawals();
            }
        }
        return total;
    }

    public int getTotalWithdrawalCharges() {
        int total = 0;
        if (getItems() != null) {
            for (Members m : getItems()) {
                total += m.getTotalWithdrawalsIncome();
            }
        }
        return total;
    }

    public int getRecoverFromMembers() {
        if (loan != null && selected != null) {
            if (loan.getBalEst() > selected.getTotalDeposit()) {
                return loan.getBalEst() - selected.getTotalDeposit();
            }
        }
        return 0;
    }

    public int getTotalForAllGuarantors() {
        int total = 0;
        if (getGuarators() != null);
        for (Guarantors g : getGuarators()) {
            total += g.getGuaranteedAmount();
        }
        return total;
    }

    public void deduct() {
        SaccoLedgerEntries sle = new SaccoLedgerEntries();
        sle.setCr(getRecoverFromMembers());
        sle.setDateWhen(new Date());
        sle.setDr(0);
        sle.setIsGeneral(true);
        sle.setParticulars("Loan Recovery From Guarantors");
        sle.setLedgerId(new SaccoLedgers(22));
        if (getRecoverFromMembers() > 0) {
            getFacade().saveLedgerEntry(sle);
        }
        sle = new SaccoLedgerEntries();
        sle.setCr(0);
        sle.setParticulars("Loan Recovery From Guarantors");
        sle.setIsGeneral(true);
        sle.setDateWhen(new Date());
        sle.setDr(getRecoverFromMembers());
        sle.setLedgerId(new SaccoLedgers(8));
        if (getRecoverFromMembers() > 0) {
            getFacade().saveLedgerEntry(sle);
        }
        Collections c = new Collections();
        if (getRecoverFromMembers() > 0 && !dummy.isEmpty()) {
            c.setAmount(loan.getBalEst());
        } else if (getRecoverFromMembers() > 0 && dummy.isEmpty()) {
            c.setAmount(selected.getTotalDeposit());
        } else if (getRecoverFromMembers() == 0) {
            c.setAmount(loan.getBalEst());
        }
        c.setCollectionstype(new CollectionTypes(loan.getLoantype().getId() + 1));
        c.setDateCollected(new Date());
        c.setMemberId(selected);
        c.setReceiptNo(11111);
        c.setStatusBilled(false);
        getFacade().storeCollections(c);
        getFacade().storeLoan(loan);
        DepositCash deposit = new DepositCash();
        if (selected.getTotalDeposit() > loan.getBalEst()) {
            deposit.setAmount(-(loan.getBalEst()));
        } else {
            deposit.setAmount(-(selected.getTotalDeposit()));
        }
        deposit.setDescription("loan recovery");
        deposit.setDate(new Date());
        deposit.setTransactedBy("");
        deposit.setPaymentMode(new PaymentModes(1));
        deposit.setChequeNo("");
        deposit.setTransactedBy("");
        deposit.setMemberNo(selected);
        if (deposit.getAmount() * -1 > 0) {
            getFacade().storeDeposit(deposit);
            sle = new SaccoLedgerEntries();
            sle.setDateWhen(new Date());
            sle.setCr(deposit.getAmount() * -1);
            sle.setDateWhen(new Date());
            sle.setDr(0);
            sle.setParticulars("Loan Recovery");
            sle.setIsGeneral(true);
            sle.setLedgerId(new SaccoLedgers(22));
            getFacade().saveLedgerEntry(sle);
            sle = new SaccoLedgerEntries();
            sle.setCr(0);
            sle.setIsGeneral(true);
            sle.setParticulars("Loan Recovery");
            sle.setDateWhen(new Date());
            sle.setDr(deposit.getAmount() * -1);
            sle.setLedgerId(new SaccoLedgers(8));
            getFacade().saveLedgerEntry(sle);
        }
        if (!dummy.isEmpty()) {
            for (Guarantors g : dummy) {
                AppConstants.logger("Recovered after" + g.getRecoveredAmount());
                deposit.setMemberNo(g.getGuarantorId());
                deposit.setAmount(-(int) g.getRecoveredAmount());
                getFacade().storeDeposit(deposit);
                LoanRecovery lr = new LoanRecovery();
                lr.setAmount((int) g.getRecoveredAmount());
                lr.setDateWhen(new Date());
                lr.setLoanId(g.getLoanId());
                lr.setMemberId(g.getGuarantorId());
                getFacade().storeLoanRecovery(lr);
            }
        }

        FacesContext.getCurrentInstance().addMessage(
                null,
                new FacesMessage(FacesMessage.SEVERITY_INFO,
                        "Loan amount has been recovered from guarantors", null));
        loan = null;
        selected = getFacade().findSelected(selected);

    }
    ArrayList<Guarantors> dummy;

    public ArrayList<Guarantors> getGuaratorsRecovery() {
        dummy = new ArrayList<>();
        if (loan != null && getRecoverFromMembers() > 0) {
            if (loan.getGuarantorsCollection() != null) {
                for (Guarantors g : loan.getGuarantorsCollection()) {
                    g.setRecoveredAmount((float) (Math.ceil((float) getRecoverFromMembers() * g.getRatio())));
                    AppConstants.logger("Recovered " + g.getRecoveredAmount());
                    dummy.add(g);
                }
            }
        }
        return dummy;
    }

    public List<Collections> getLoanCollections() {
        if (getLoan().getLoantype().getId() == 1) {
            return getFacade().getCollections(2, getLoan());
        } else {
            return getFacade().getCollections(3, getLoan());
        }
    }

    public void onPrintCollections() throws JRException, IOException {
        JasperPrint jasperPrint;
        AppConstants.logger("calling print");
        ArrayList<Collections> finallist = new ArrayList<>();
        int x = 1;
        for (Collections c : getCollectionsBetweenDates()) {
            c.setId(x);
            x++;
            finallist.add(c);
        }
        JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(finallist);
        FacesContext facesContext = FacesContext.getCurrentInstance();
        String reportPath = facesContext.getExternalContext().getRealPath("/report/collections.jasper");
        Map params = new HashMap();
        params.put("reportsdate", "MEMBERS COLLECTIONS FOR " + selected.getSurname() + " MEMBER NO:" + selected.getMemberNo() + " BETWEEN " + new SimpleDateFormat("dd-MM-yyyy").format(sdate) + " TO " + new SimpleDateFormat("dd-MM-yyyy").format(edate) + " RECEIPT TYPE:" + this.type.getType().toUpperCase());
        params.put("total", getTotalCollectionsBetweenDates());
        AppConstants.logger("params" + params.toString());
        jasperPrint = JasperFillManager.fillReport(reportPath, params, beanCollectionDataSource);
        HttpServletResponse httpServletResponse = (HttpServletResponse) facesContext.getExternalContext().getResponse();
        httpServletResponse.addHeader("Content-disposition", "attachment; filename=" + selected.getTrimmedSurname() + ".pdf");
        try (ServletOutputStream servletOutputStream = httpServletResponse.getOutputStream()) {
            JasperExportManager.exportReportToPdfStream(jasperPrint, servletOutputStream);
            servletOutputStream.flush();
        }
        FacesContext.getCurrentInstance().responseComplete();
    }

    public void onPrintCollectionsAll() throws IOException, JRException {
        JasperPrint jasperPrint;
        AppConstants.logger("calling print");
        ArrayList<Collections> finallist = new ArrayList<>();
        int x = 1;
        for (Collections c : getCollectionsBetweenDatesAll()) {
            c.setId(x);
            x++;
            finallist.add(c);
        }
        JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(finallist);
        FacesContext facesContext = FacesContext.getCurrentInstance();
        String reportPath = facesContext.getExternalContext().getRealPath("/report/CollectionWithTypejrxml.jasper");
        Map params = new HashMap();
        params.put("reportsdate", "MEMBERS COLLECTIONS FOR " + selected.getSurname() + " MEMBER NO:" + selected.getMemberNo() + " BETWEEN " + new SimpleDateFormat("dd-MM-yyyy").format(sdate) + " TO " + new SimpleDateFormat("dd-MM-yyyy").format(edate) + " ALL RECEIPT TYPES");
        params.put("total", getTotalCollectionsBetweenDatesAll());
        AppConstants.logger("params" + params.toString());
        jasperPrint = JasperFillManager.fillReport(reportPath, params, beanCollectionDataSource);
        HttpServletResponse httpServletResponse = (HttpServletResponse) facesContext.getExternalContext().getResponse();
        httpServletResponse.addHeader("Content-disposition", "attachment; filename=" + selected.getTrimmedSurname() + ".pdf");
        try (ServletOutputStream servletOutputStream = httpServletResponse.getOutputStream()) {
            JasperExportManager.exportReportToPdfStream(jasperPrint, servletOutputStream);
            servletOutputStream.flush();
        }
        FacesContext.getCurrentInstance().responseComplete();
    }

    public void onPrint() throws IOException, JRException {
        JasperPrint jasperPrint;
        AppConstants.logger("calling print");
        ArrayList<PaymentSummaryItems> finallist = new ArrayList<>();
        int x = 1;
        for (PaymentSummaryItems pt : selected.getPaymentSummaryItemsCollection()) {
            pt.setId(x);
            x++;
            finallist.add(pt);
        }
        JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(finallist);
        FacesContext facesContext = FacesContext.getCurrentInstance();
        String reportPath = facesContext.getExternalContext().getRealPath("/admin/report/statement.jasper");
        Map params = new HashMap();
        params.put("reportsdate", "MEMBER NAME:" + selected.getSurname() + ".  MEMBER NO: " + selected.getMemberNo() + ". SOCIETY: T.K.N SACCO.   STATEMENT AS AT: " + finallist.get(finallist.size() - 1).getDatePrepared() + ".");
        AppConstants.logger("params" + params.toString());
        jasperPrint = JasperFillManager.fillReport(reportPath, params, beanCollectionDataSource);
        HttpServletResponse httpServletResponse = (HttpServletResponse) facesContext.getExternalContext().getResponse();
        httpServletResponse.addHeader("Content-disposition", "attachment; filename=" + selected.getTrimmedSurname() + ".pdf");
        try (ServletOutputStream servletOutputStream = httpServletResponse.getOutputStream()) {
            JasperExportManager.exportReportToPdfStream(jasperPrint, servletOutputStream);
            servletOutputStream.flush();
        }
        FacesContext.getCurrentInstance().responseComplete();
    }

    public void createWithDrawal() {
        depo.setAmount(depo.getAmount() * -1);
        depo.setChequeNo("");
        depo.setPaymentMode(new PaymentModes(1));
        depo.setMemberNo(selected);
        depo.setTransactedBy("");
        depo.setDate(new Date());
        getFacade().storeDeposit(depo);
        FacesContext.getCurrentInstance().addMessage(
                null,
                new FacesMessage(FacesMessage.SEVERITY_INFO,
                        "Withdrawal successiful", null));
        depo = null;
        selected = getFacade().findSelected(selected);
    }

    public void stopBillingLoan() {
        loan.setStopBilling(true);
        getFacade().storeLoan(loan);
        JsfUtil.addSuccessMessage("Loan interest billing stoped");
        updateMember();
    }

    public void startBillingLoan() {
        loan.setStopBilling(false);
        getFacade().storeLoan(loan);
        JsfUtil.addSuccessMessage("Loan interest billing restarted");
        updateMember();
    }

    public void loanCleared() {
        loan.setLoanStatus(true);
        getFacade().mergeLoan(loan);
        JsfUtil.addSuccessMessage("Loan cleared");
    }
}
