package controllers;

import controllers.util.AppConstants;
import controllers.util.JsfUtil;
import controllers.util.JsfUtil.PersistAction;
import entities.Guarantors;
import entities.Loan;
import entities.LoanScheduleTransactions;
import entities.LoanSchedules;
import entities.ScheduleStatus;
import facades.LoanFacade;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Named;
import org.joda.time.DateTime;
import org.primefaces.event.FlowEvent;
import org.primefaces.event.RowEditEvent;

@Named("loanController")
@SessionScoped
public class LoanController implements Serializable {

    @EJB
    private LoanFacade ejbFacade;
    private List<Loan> items = null;
    private Loan selected;
    private int noOfGuarantors = 5;
    private ArrayList<Guarantors> list = new ArrayList<>();

    public LoanController() {
    }

    public Loan getSelected() {
        return selected;
    }

    public void setSelected(Loan selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private LoanFacade getFacade() {
        return ejbFacade;
    }

    public int getNoOfGuarantors() {
        return noOfGuarantors;
    }

    public void editAllLoans() {
        for (Loan loan : getItems()) {
            AppConstants.logger("Loan Amount:" + loan.getAmount());
            AppConstants.logger("Loan member Name:" + loan.getMemeberId().getSurname());
            int random = new Random().nextInt(10000);
            int interest = (int) Math.ceil(loan.getAmount() * loan.getInterestRate() / 12 * 0.01);
            LoanSchedules item = new LoanSchedules();
            item.setPrincipleAmount((float) loan.getAmount());
            item.setInterest(interest);
            item.setBalance((float) loan.getAmount());
            item.setLoanId(loan);
            item.setStatusCleared(new ScheduleStatus(2));
            DateTime today = new DateTime();
            DateTime nextAfterAMonth = today.plusMonths(1);
            Date objectDate = nextAfterAMonth.toDate();
            item.setDueDate(objectDate);
            item.setIsForLastMonth(true);
            item.setRandomInt(random);
            getFacade().storeLoanSchedule(item);
            item = getFacade().getLoanSchedule(random);
            item.setRandomInt(-1);
            getFacade().mergeLoanSchedule(item);
            LoanScheduleTransactions lst = new LoanScheduleTransactions();
            lst.setAmountDebit(interest);
            lst.setAmountCredit(0.0);
            lst.setDateCollected(new Date());
            lst.setDetails("interest");
            lst.setScheduleId(item);
            getFacade().storeTransaction(lst);
        }
    }

    public ArrayList<Guarantors> getGuarators() {
        if (list.isEmpty()) {
            for (int x = 0; x < noOfGuarantors; x++) {
                Guarantors g = new Guarantors();
                g.setId(x + 1);
                g.setGuaranteedAmount(0);
                list.add(g);
            }
            return list;
        } else {
            return list;
        }
    }

    public void onRowEdit(RowEditEvent event) {
        Guarantors newg = (Guarantors) event.getObject();
        list.get(newg.getId() - 1).setGuaranteedAmount(newg.getGuaranteedAmount());
        list.get(newg.getId() - 1).setGuarantorId(newg.getGuarantorId());
    }

    public void onRowCancel(RowEditEvent event) {
        AppConstants.logger("yeiuyuieyiru");
        FacesContext.getCurrentInstance().addMessage(
                null,
                new FacesMessage(FacesMessage.SEVERITY_FATAL,
                        "No record have been saved", null));
    }

    public void setNoOfGuarantors(int noOfGuarantors) {
        this.noOfGuarantors = noOfGuarantors;
    }

    public Loan prepareCreate() {
        selected = new Loan();
        initializeEmbeddableKey();
        list = new ArrayList<>();
        return selected;
    }

    public void create() {
        int rand = new Random().nextInt(100000000);
        selected.setRandomInt(rand);
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("LoanCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("LoanUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("LoanDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public String onFlowProcess(FlowEvent event) {
        return event.getNewStep();
    }

    public List<Loan> getItemsDefaulted() {
        items = getFacade().findAllDefaulted();
        return items;
    }

    public List<Loan> getItems() {
        items = getFacade().findAll();
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }

    public Loan getLoan(java.lang.Integer id) {
        return getFacade().find(id);
    }

    public List<Loan> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<Loan> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    @FacesConverter(forClass = Loan.class)
    public static class LoanControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            LoanController controller = (LoanController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "loanController");
            return controller.getLoan(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Loan) {
                Loan o = (Loan) object;
                return getStringKey(o.getLoanId());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Loan.class.getName()});
                return null;
            }
        }
    }

    public int getTotalLoansDefaulted() {
        int total = 0;
        for (Loan l : getItemsDefaulted()) {
            total += l.getAmount();
        }
        return total;
    }

    public int getTotalLoansBalancesDefaulted() {
        int total = 0;
        for (Loan l : getItemsDefaulted()) {
            total += l.getBalEst();
        }
        return total;
    }

    public int getTotalLoans() {
        int total = 0;
        for (Loan l : getItems()) {
            total += l.getAmount();
        }
        return total;
    }

    public int getTotalLoansNegotiation() {
        int total = 0;
        for (Loan l : getItems()) {
            total += l.getNegotiationFee();
        }
        return total;
    }

    public int getTotalLoansInterest() {
        int total = 0;
        for (Loan l : getItems()) {
            total += l.getTotalInterestEarned();
        }
        return total;
    }

    public int getTotalLoansBalances() {
        int total = 0;
        for (Loan l : getItems()) {
            total += l.getBalEst();
        }
        return total;
    }

    public int getTotalLoansBalancesWithoutInterest() {
        int total = 0;
        for (Loan l : getItems()) {
            total += l.getBalEstWithoutInterest();
        }
        return total;
    }

    public String getCurrentYear() {
        return new SimpleDateFormat("yyyy").format(new Date());
    }

    public String getTitle() {
        return "LOANS GRANTED IN THE YEAR " + getCurrentYear();
    }

}
