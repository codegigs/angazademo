package controllers;

import controllers.util.AppConstants;
import controllers.util.JsfUtil;
import controllers.util.JsfUtil.PersistAction;
import entities.Expenses;
import entities.SaccoLedgerEntries;
import entities.SaccoLedgers;
import facades.ExpensesFacade;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Named;

@Named("expensesController")
@SessionScoped
public class ExpensesController implements Serializable {

    @EJB
    private facades.ExpensesFacade ejbFacade;
    private List<Expenses> items = null;
    private Expenses selected;

    public ExpensesController() {
    }

    public Expenses getSelected() {
        return selected;
    }

    public void setSelected(Expenses selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private ExpensesFacade getFacade() {
        return ejbFacade;
    }

    public Expenses prepareCreate() {
        selected = new Expenses();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("ExpensesCreated"));
        if (!JsfUtil.isValidationFailed()) {
            SaccoLedgers sl = getFacade().getSaccoLedgerByTitle(selected.getNarration().getTitle());
            SaccoLedgerEntries sle = new SaccoLedgerEntries();
            sle.setCr(0);
            sle.setDateWhen(new Date());
            sle.setDr(selected.getAmount());
            sle.setLedgerId(sl);
            getFacade().saveLedgerEntry(sle);
            if (selected.getPaymentMethod().getId() == 1) {
                AppConstants.logger("saving cash");
                sle = new SaccoLedgerEntries();
                sle.setCr(selected.getAmount());
                sle.setDateWhen(new Date());
                sle.setDr(0);
                sle.setLedgerId(new SaccoLedgers(6));
                getFacade().saveLedgerEntry(sle);
            }
            else{
                AppConstants.logger("saving bank");
                sle = new SaccoLedgerEntries();
                sle.setCr(selected.getAmount());
                sle.setDateWhen(new Date());
                sle.setDr(0);
                sle.setLedgerId(new SaccoLedgers(7));
                getFacade().saveLedgerEntry(sle);
            }
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("ExpensesUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("ExpensesDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<Expenses> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }

    public Expenses getExpenses(java.lang.Integer id) {
        return getFacade().find(id);
    }

    public List<Expenses> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<Expenses> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    @FacesConverter(forClass = Expenses.class)
    public static class ExpensesControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            ExpensesController controller = (ExpensesController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "expensesController");
            return controller.getExpenses(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Expenses) {
                Expenses o = (Expenses) object;
                return getStringKey(o.getId());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Expenses.class.getName()});
                return null;
            }
        }

    }

}
