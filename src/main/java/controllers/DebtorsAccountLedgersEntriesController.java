package controllers;

import entities.DebtorsAccountLedgersEntries;
import controllers.util.JsfUtil;
import controllers.util.JsfUtil.PersistAction;
import facades.DebtorsAccountLedgersEntriesFacade;

import java.io.Serializable;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@Named("debtorsAccountLedgersEntriesController")
@SessionScoped
public class DebtorsAccountLedgersEntriesController implements Serializable {

    @EJB
    private facades.DebtorsAccountLedgersEntriesFacade ejbFacade;
    private List<DebtorsAccountLedgersEntries> items = null;
    private DebtorsAccountLedgersEntries selected;

    public DebtorsAccountLedgersEntriesController() {
    }

    public DebtorsAccountLedgersEntries getSelected() {
        return selected;
    }

    public void setSelected(DebtorsAccountLedgersEntries selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private DebtorsAccountLedgersEntriesFacade getFacade() {
        return ejbFacade;
    }

    public DebtorsAccountLedgersEntries prepareCreate() {
        selected = new DebtorsAccountLedgersEntries();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("DebtorsAccountLedgersEntriesCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("DebtorsAccountLedgersEntriesUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("DebtorsAccountLedgersEntriesDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<DebtorsAccountLedgersEntries> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }

    public DebtorsAccountLedgersEntries getDebtorsAccountLedgersEntries(java.lang.Integer id) {
        return getFacade().find(id);
    }

    public List<DebtorsAccountLedgersEntries> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<DebtorsAccountLedgersEntries> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    @FacesConverter(forClass = DebtorsAccountLedgersEntries.class)
    public static class DebtorsAccountLedgersEntriesControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            DebtorsAccountLedgersEntriesController controller = (DebtorsAccountLedgersEntriesController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "debtorsAccountLedgersEntriesController");
            return controller.getDebtorsAccountLedgersEntries(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof DebtorsAccountLedgersEntries) {
                DebtorsAccountLedgersEntries o = (DebtorsAccountLedgersEntries) object;
                return getStringKey(o.getId());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), DebtorsAccountLedgersEntries.class.getName()});
                return null;
            }
        }

    }

}
