/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import entities.Members;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author francis
 */
@Stateless
@Path("entities.members")
public class MembersFacadeREST extends AbstractFacade<Members> {

    @PersistenceContext(unitName = "SaccoManagement")
    private EntityManager em;

    public MembersFacadeREST() {
        super(Members.class);
    }
    @GET
    @Path("/all")
    @Override
    @Produces({MediaType.APPLICATION_JSON})
    public List<Members> findAll() {
        return super.findAll();
    }
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
