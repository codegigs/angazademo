/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author francis
 */
@Entity
@Table(name = "bill_transactions")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BillTransactions.findAll", query = "SELECT b FROM BillTransactions b"),
    @NamedQuery(name = "BillTransactions.findById", query = "SELECT b FROM BillTransactions b WHERE b.id = :id"),
    @NamedQuery(name = "BillTransactions.findByAmountDebit", query = "SELECT b FROM BillTransactions b WHERE b.amountDebit = :amountDebit"),
    @NamedQuery(name = "BillTransactions.findByDatePaid", query = "SELECT b FROM BillTransactions b WHERE b.datePaid = :datePaid"),
    @NamedQuery(name = "BillTransactions.findByAmountCredit", query = "SELECT b FROM BillTransactions b WHERE b.amountCredit = :amountCredit"),
    @NamedQuery(name = "BillTransactions.findByNaration", query = "SELECT b FROM BillTransactions b WHERE b.naration = :naration"),
    @NamedQuery(name = "BillTransactions.findByReceiptNo", query = "SELECT b FROM BillTransactions b WHERE b.receiptNo = :receiptNo")})
public class BillTransactions implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "status_billed")
    private boolean statusBilled;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "amount_debit")
    private int amountDebit;
    @Basic(optional = false)
    @NotNull
    @Column(name = "date_paid")
    @Temporal(TemporalType.DATE)
    private Date datePaid;
    @Basic(optional = false)
    @NotNull
    @Column(name = "amount_credit")
    private int amountCredit;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "naration")
    private String naration;
    @Size(max = 45)
    @Column(name = "receipt_no")
    private String receiptNo;
    @JoinColumn(name = "bill_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private MemberBills billId;

    public BillTransactions() {
    }

    public BillTransactions(Integer id) {
        this.id = id;
    }

    public BillTransactions(Integer id, int amountDebit, Date datePaid, int amountCredit, String naration) {
        this.id = id;
        this.amountDebit = amountDebit;
        this.datePaid = datePaid;
        this.amountCredit = amountCredit;
        this.naration = naration;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getAmountDebit() {
        return amountDebit;
    }

    public void setAmountDebit(int amountDebit) {
        this.amountDebit = amountDebit;
    }

    public Date getDatePaid() {
        return datePaid;
    }

    public void setDatePaid(Date datePaid) {
        this.datePaid = datePaid;
    }

    public int getAmountCredit() {
        return amountCredit;
    }

    public void setAmountCredit(int amountCredit) {
        this.amountCredit = amountCredit;
    }

    public String getNaration() {
        return naration;
    }

    public void setNaration(String naration) {
        this.naration = naration;
    }

    public String getReceiptNo() {
        return receiptNo;
    }

    public void setReceiptNo(String receiptNo) {
        this.receiptNo = receiptNo;
    }

    public MemberBills getBillId() {
        return billId;
    }

    public void setBillId(MemberBills billId) {
        this.billId = billId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BillTransactions)) {
            return false;
        }
        BillTransactions other = (BillTransactions) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.BillTransactions[ id=" + id + " ]";
    }

    public boolean getStatusBilled() {
        return statusBilled;
    }

    public void setStatusBilled(boolean statusBilled) {
        this.statusBilled = statusBilled;
    }
    
}
