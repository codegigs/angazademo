/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author francis
 */
@Entity
@Table(name = "sacco_income")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SaccoIncome.findAll", query = "SELECT s FROM SaccoIncome s"),
    @NamedQuery(name = "SaccoIncome.findByTransactionNo", query = "SELECT s FROM SaccoIncome s WHERE s.transactionNo = :transactionNo"),
    @NamedQuery(name = "SaccoIncome.findByDateCollected", query = "SELECT s FROM SaccoIncome s WHERE s.dateCollected = :dateCollected"),
    @NamedQuery(name = "SaccoIncome.findByAmount", query = "SELECT s FROM SaccoIncome s WHERE s.amount = :amount"),
    @NamedQuery(name = "SaccoIncome.findById", query = "SELECT s FROM SaccoIncome s WHERE s.id = :id")})
public class SaccoIncome implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "transaction_no")
    private String transactionNo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "date_collected")
    @Temporal(TemporalType.DATE)
    private Date dateCollected;
    @Basic(optional = false)
    @NotNull
    @Column(name = "amount")
    private int amount;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @JoinColumn(name = "source", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private SaccoItemsSellable source;

    public SaccoIncome() {
    }

    public SaccoIncome(Integer id) {
        this.id = id;
    }

    public SaccoIncome(Integer id, String transactionNo, Date dateCollected, int amount) {
        this.id = id;
        this.transactionNo = transactionNo;
        this.dateCollected = dateCollected;
        this.amount = amount;
    }

    public String getTransactionNo() {
        return transactionNo;
    }

    public void setTransactionNo(String transactionNo) {
        this.transactionNo = transactionNo;
    }

    public Date getDateCollected() {
        return dateCollected;
    }

    public void setDateCollected(Date dateCollected) {
        this.dateCollected = dateCollected;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public SaccoItemsSellable getSource() {
        return source;
    }

    public void setSource(SaccoItemsSellable source) {
        this.source = source;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SaccoIncome)) {
            return false;
        }
        SaccoIncome other = (SaccoIncome) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.SaccoIncome[ id=" + id + " ]";
    }
    
}
