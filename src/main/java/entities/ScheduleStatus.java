/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author francis
 */
@Entity
@Table(name = "schedule_status")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ScheduleStatus.findAll", query = "SELECT s FROM ScheduleStatus s"),
    @NamedQuery(name = "ScheduleStatus.findByIdscheduleStatus", query = "SELECT s FROM ScheduleStatus s WHERE s.idscheduleStatus = :idscheduleStatus"),
    @NamedQuery(name = "ScheduleStatus.findByStatus", query = "SELECT s FROM ScheduleStatus s WHERE s.status = :status")})
public class ScheduleStatus implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idschedule_status")
    private Integer idscheduleStatus;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "status")
    private String status;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "statusCleared")
    private Collection<LoanSchedules> loanSchedulesCollection;

    public ScheduleStatus() {
    }

    public ScheduleStatus(Integer idscheduleStatus) {
        this.idscheduleStatus = idscheduleStatus;
    }

    public ScheduleStatus(Integer idscheduleStatus, String status) {
        this.idscheduleStatus = idscheduleStatus;
        this.status = status;
    }

    public Integer getIdscheduleStatus() {
        return idscheduleStatus;
    }

    public void setIdscheduleStatus(Integer idscheduleStatus) {
        this.idscheduleStatus = idscheduleStatus;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @XmlTransient
    public Collection<LoanSchedules> getLoanSchedulesCollection() {
        return loanSchedulesCollection;
    }

    public void setLoanSchedulesCollection(Collection<LoanSchedules> loanSchedulesCollection) {
        this.loanSchedulesCollection = loanSchedulesCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idscheduleStatus != null ? idscheduleStatus.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ScheduleStatus)) {
            return false;
        }
        ScheduleStatus other = (ScheduleStatus) object;
        if ((this.idscheduleStatus == null && other.idscheduleStatus != null) || (this.idscheduleStatus != null && !this.idscheduleStatus.equals(other.idscheduleStatus))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.ScheduleStatus[ idscheduleStatus=" + idscheduleStatus + " ]";
    }
    
}
