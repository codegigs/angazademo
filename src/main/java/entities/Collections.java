/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author francis
 */
@Entity
@Table(name = "collections")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Collections.findAll", query = "SELECT c FROM Collections c ORDER BY c.dateCollected DESC"),
    @NamedQuery(name = "Collections.findById", query = "SELECT c FROM Collections c WHERE c.id = :id"),
    @NamedQuery(name = "Collections.findByAmount", query = "SELECT c FROM Collections c WHERE c.amount = :amount"),
    @NamedQuery(name = "Collections.findByDateCollected", query = "SELECT c FROM Collections c WHERE c.dateCollected = :dateCollected"),
    @NamedQuery(name = "Collections.findByDateCollectedAndType", query = "SELECT c FROM Collections c WHERE c.dateCollected = :dateCollected AND c.collectionstype = :type"),
    @NamedQuery(name = "Collections.findByStatusBilled", query = "SELECT c FROM Collections c WHERE c.statusBilled = :statusBilled")})
public class Collections implements Serializable {

    @Column(name = "random_int")
    private Integer randomInt;

    @Basic(optional = false)
    @NotNull
    @Column(name = "receipt_no")
    private int receiptNo;
    @JoinColumn(name = "collectionstype", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private CollectionTypes collectionstype;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "amount")
    private int amount;
    @Basic(optional = false)
    @NotNull
    @Column(name = "date_collected")
    @Temporal(TemporalType.DATE)
    private Date dateCollected = new Date();
    @Basic(optional = false)
    @NotNull
    @Column(name = "status_billed")
    private boolean statusBilled;
    @JoinColumn(name = "member_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Members memberId;

    public Collections() {
    }

    public Collections(Integer id) {
        this.id = id;
    }

    public Collections(Integer id, int amount, Date dateCollected, boolean statusBilled) {
        this.id = id;
        this.amount = amount;
        this.dateCollected = dateCollected;
        this.statusBilled = statusBilled;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public Date getDateCollected() {
        return dateCollected;
    }

    public void setDateCollected(Date dateCollected) {
        this.dateCollected = dateCollected;
    }

    public boolean getStatusBilled() {
        return statusBilled;
    }

    public void setStatusBilled(boolean statusBilled) {
        this.statusBilled = statusBilled;
    }

    public Members getMemberId() {
        return memberId;
    }

    public void setMemberId(Members memberId) {
        this.memberId = memberId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Collections)) {
            return false;
        }
        Collections other = (Collections) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Collections{" + "randomInt=" + randomInt + ", receiptNo=" + receiptNo + ", collectionstype=" + collectionstype + ", id=" + id + ", amount=" + amount + ", dateCollected=" + dateCollected + ", statusBilled=" + statusBilled + ", memberId=" + memberId + '}';
    }

    public CollectionTypes getCollectionstype() {
        return collectionstype;
    }

    public void setCollectionstype(CollectionTypes collectionstype) {
        this.collectionstype = collectionstype;
    }

    public int getReceiptNo() {
        return receiptNo;
    }

    public void setReceiptNo(int receiptNo) {
        this.receiptNo = receiptNo;
    }

    public Integer getRandomInt() {
        return randomInt;
    }

    public void setRandomInt(Integer randomInt) {
        this.randomInt = randomInt;
    }
    public static Comparator<Collections> Total = new Comparator<Collections>() {

        @Override
        public int compare(Collections t, Collections t1) {
            int id = t.getId();
            int id1 = t1.getId();
            return id - id1;
        }
    };
    
    
    public String getCollectionDateStr(){
        return new SimpleDateFormat("dd-MM-yyyy").format(getDateCollected());
    }
}
