/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author francis
 */
@Entity
@Table(name = "gender")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Gender.findAll", query = "SELECT g FROM Gender g"),
    @NamedQuery(name = "Gender.findByIdgender", query = "SELECT g FROM Gender g WHERE g.idgender = :idgender"),
    @NamedQuery(name = "Gender.findByGender", query = "SELECT g FROM Gender g WHERE g.gender = :gender")})
public class Gender implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "idgender")
    private Integer idgender;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "gender")
    private String gender;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "gender")
    private Collection<Members> membersCollection;

    public Gender() {
    }

    public Gender(Integer idgender) {
        this.idgender = idgender;
    }

    public Gender(Integer idgender, String gender) {
        this.idgender = idgender;
        this.gender = gender;
    }

    public Integer getIdgender() {
        return idgender;
    }

    public void setIdgender(Integer idgender) {
        this.idgender = idgender;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    @XmlTransient
    public Collection<Members> getMembersCollection() {
        return membersCollection;
    }

    public void setMembersCollection(Collection<Members> membersCollection) {
        this.membersCollection = membersCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idgender != null ? idgender.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Gender)) {
            return false;
        }
        Gender other = (Gender) object;
        if ((this.idgender == null && other.idgender != null) || (this.idgender != null && !this.idgender.equals(other.idgender))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Gender[ idgender=" + idgender + " ]";
    }
    
}
