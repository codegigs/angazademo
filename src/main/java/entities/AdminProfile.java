package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "admin_profile")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AdminProfile.findAll", query = "SELECT a FROM AdminProfile a"),
    @NamedQuery(name = "AdminProfile.findById", query = "SELECT a FROM AdminProfile a WHERE a.id = :id"),
    @NamedQuery(name = "AdminProfile.findByName", query = "SELECT a FROM AdminProfile a WHERE a.name = :name"),
    @NamedQuery(name = "AdminProfile.findByPhoneNumber", query = "SELECT a FROM AdminProfile a WHERE a.phoneNumber = :phoneNumber"),
    @NamedQuery(name = "AdminProfile.findByIdNumber", query = "SELECT a FROM AdminProfile a WHERE a.idNumber = :idNumber"),
    @NamedQuery(name = "AdminProfile.findByStatusActive", query = "SELECT a FROM AdminProfile a WHERE a.statusActive = :statusActive"),
    @NamedQuery(name = "AdminProfile.findByDeleted", query = "SELECT a FROM AdminProfile a WHERE a.deleted = :deleted")})
public class AdminProfile implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "phone_number")
    private String phoneNumber;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "id_number")
    private String idNumber;
    @Basic(optional = false)
    @NotNull
    @Column(name = "status_active")
    private boolean statusActive;
    @Basic(optional = false)
    @NotNull
    @Column(name = "deleted")
    private boolean deleted;
    @JoinColumn(name = "username", referencedColumnName = "username")
    @ManyToOne(optional = false)
    private Users username;

    public AdminProfile() {
    }

    public AdminProfile(Integer id) {
        this.id = id;
    }

    public AdminProfile(Integer id, String name, String phoneNumber, String idNumber, boolean statusActive, boolean deleted) {
        this.id = id;
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.idNumber = idNumber;
        this.statusActive = statusActive;
        this.deleted = deleted;
    }

    public String getStatus() {
        if (getStatusActive()) {
            return "Active";
        }
        return "Inactive";
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public boolean getStatusActive() {
        return statusActive;
    }

    public void setStatusActive(boolean statusActive) {
        this.statusActive = statusActive;
    }

    public boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Users getUsername() {
        return username;
    }

    public void setUsername(Users username) {
        this.username = username;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AdminProfile)) {
            return false;
        }
        AdminProfile other = (AdminProfile) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.AdminProfile[ id=" + id + " ]";
    }

}
