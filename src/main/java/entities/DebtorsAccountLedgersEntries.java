/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author francis
 */
@Entity
@Table(name = "debtors_account_ledgers_entries")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DebtorsAccountLedgersEntries.findAll", query = "SELECT d FROM DebtorsAccountLedgersEntries d"),
    @NamedQuery(name = "DebtorsAccountLedgersEntries.findById", query = "SELECT d FROM DebtorsAccountLedgersEntries d WHERE d.id = :id"),
    @NamedQuery(name = "DebtorsAccountLedgersEntries.findByDateWhen", query = "SELECT d FROM DebtorsAccountLedgersEntries d WHERE d.dateWhen = :dateWhen"),
    @NamedQuery(name = "DebtorsAccountLedgersEntries.findByDr", query = "SELECT d FROM DebtorsAccountLedgersEntries d WHERE d.dr = :dr"),
    @NamedQuery(name = "DebtorsAccountLedgersEntries.findByCr", query = "SELECT d FROM DebtorsAccountLedgersEntries d WHERE d.cr = :cr")})
public class DebtorsAccountLedgersEntries implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "date_when")
    @Temporal(TemporalType.DATE)
    private Date dateWhen;
    @Basic(optional = false)
    @NotNull
    @Column(name = "dr")
    private int dr;
    @Basic(optional = false)
    @NotNull
    @Column(name = "cr")
    private int cr;
    @JoinColumn(name = "debtors_account_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Deptors debtorsAccountId;

    public DebtorsAccountLedgersEntries() {
    }

    public DebtorsAccountLedgersEntries(Integer id) {
        this.id = id;
    }

    public DebtorsAccountLedgersEntries(Integer id, Date dateWhen, int dr, int cr) {
        this.id = id;
        this.dateWhen = dateWhen;
        this.dr = dr;
        this.cr = cr;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDateWhen() {
        return dateWhen;
    }

    public void setDateWhen(Date dateWhen) {
        this.dateWhen = dateWhen;
    }

    public int getDr() {
        return dr;
    }

    public void setDr(int dr) {
        this.dr = dr;
    }

    public int getCr() {
        return cr;
    }

    public void setCr(int cr) {
        this.cr = cr;
    }

    public Deptors getDebtorsAccountId() {
        return debtorsAccountId;
    }

    public void setDebtorsAccountId(Deptors debtorsAccountId) {
        this.debtorsAccountId = debtorsAccountId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DebtorsAccountLedgersEntries)) {
            return false;
        }
        DebtorsAccountLedgersEntries other = (DebtorsAccountLedgersEntries) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.DebtorsAccountLedgersEntries[ id=" + id + " ]";
    }
    
}
