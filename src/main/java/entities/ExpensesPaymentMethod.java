/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author francis
 */
@Entity
@Table(name = "expenses_payment_method")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ExpensesPaymentMethod.findAll", query = "SELECT e FROM ExpensesPaymentMethod e"),
    @NamedQuery(name = "ExpensesPaymentMethod.findById", query = "SELECT e FROM ExpensesPaymentMethod e WHERE e.id = :id"),
    @NamedQuery(name = "ExpensesPaymentMethod.findByMethod", query = "SELECT e FROM ExpensesPaymentMethod e WHERE e.method = :method")})
public class ExpensesPaymentMethod implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "method")
    private String method;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "paymentMethod")
    private Collection<Expenses> expensesCollection;

    public ExpensesPaymentMethod() {
    }

    public ExpensesPaymentMethod(Integer id) {
        this.id = id;
    }

    public ExpensesPaymentMethod(Integer id, String method) {
        this.id = id;
        this.method = method;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    @XmlTransient
    public Collection<Expenses> getExpensesCollection() {
        return expensesCollection;
    }

    public void setExpensesCollection(Collection<Expenses> expensesCollection) {
        this.expensesCollection = expensesCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ExpensesPaymentMethod)) {
            return false;
        }
        ExpensesPaymentMethod other = (ExpensesPaymentMethod) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.ExpensesPaymentMethod[ id=" + id + " ]";
    }
    
}
