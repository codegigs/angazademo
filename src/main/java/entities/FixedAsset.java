/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author francis
 */
@Entity
@Table(name = "fixed_asset")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FixedAsset.findAll", query = "SELECT f FROM FixedAsset f"),
    @NamedQuery(name = "FixedAsset.findById", query = "SELECT f FROM FixedAsset f WHERE f.id = :id"),
    @NamedQuery(name = "FixedAsset.findByName", query = "SELECT f FROM FixedAsset f WHERE f.name = :name"),
    @NamedQuery(name = "FixedAsset.findByValue", query = "SELECT f FROM FixedAsset f WHERE f.value = :value"),
    @NamedQuery(name = "FixedAsset.findByDepreciationPerYear", query = "SELECT f FROM FixedAsset f WHERE f.depreciationPerYear = :depreciationPerYear"),
    @NamedQuery(name = "FixedAsset.findByDescription", query = "SELECT f FROM FixedAsset f WHERE f.description = :description"),
    @NamedQuery(name = "FixedAsset.findByDateAcquired", query = "SELECT f FROM FixedAsset f WHERE f.dateAcquired = :dateAcquired")})
public class FixedAsset implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Column(name = "value")
    private int value;
    @Basic(optional = false)
    @NotNull
    @Column(name = "depreciation_per_year")
    private int depreciationPerYear;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "description")
    private String description;
    @Basic(optional = false)
    @NotNull
    @Column(name = "date_acquired")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dateAcquired;

    public FixedAsset() {
    }

    public FixedAsset(Integer id) {
        this.id = id;
    }

    public FixedAsset(Integer id, String name, int value, int depreciationPerYear, String description, Date dateAcquired) {
        this.id = id;
        this.name = name;
        this.value = value;
        this.depreciationPerYear = depreciationPerYear;
        this.description = description;
        this.dateAcquired = dateAcquired;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getDepreciationPerYear() {
        return depreciationPerYear;
    }

    public void setDepreciationPerYear(int depreciationPerYear) {
        this.depreciationPerYear = depreciationPerYear;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDateAcquired() {
        return dateAcquired;
    }

    public void setDateAcquired(Date dateAcquired) {
        this.dateAcquired = dateAcquired;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FixedAsset)) {
            return false;
        }
        FixedAsset other = (FixedAsset) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.FixedAsset[ id=" + id + " ]";
    }
    
}
