/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author francis
 */
@Entity
@Table(name = "loan_types")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LoanTypes.findAll", query = "SELECT l FROM LoanTypes l"),
    @NamedQuery(name = "LoanTypes.findById", query = "SELECT l FROM LoanTypes l WHERE l.id = :id"),
    @NamedQuery(name = "LoanTypes.findByLoanType", query = "SELECT l FROM LoanTypes l WHERE l.loanType = :loanType")})
public class LoanTypes implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "loan_type")
    private String loanType;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "loantype")
    private Collection<Loan> loanCollection;

    public LoanTypes() {
    }

    public LoanTypes(Integer id) {
        this.id = id;
    }

    public LoanTypes(Integer id, String loanType) {
        this.id = id;
        this.loanType = loanType;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLoanType() {
        return loanType;
    }

    public void setLoanType(String loanType) {
        this.loanType = loanType;
    }

    @XmlTransient
    public Collection<Loan> getLoanCollection() {
        return loanCollection;
    }

    public void setLoanCollection(Collection<Loan> loanCollection) {
        this.loanCollection = loanCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LoanTypes)) {
            return false;
        }
        LoanTypes other = (LoanTypes) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "LoanTypes{" + "id=" + id + ", loanType=" + loanType + '}';
    }

}
