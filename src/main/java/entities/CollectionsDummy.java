/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

/**
 *
 * @author francis
 */
public class CollectionsDummy {
    Members m;
    int savings; int emergency;int normal;
    String receiptsavings,receiptEmergency, receiptNormal;

    public Members getM() {
        return m;
    }

    public void setM(Members m) {
        this.m = m;
    }

    public int getSavings() {
        return savings;
    }

    public void setSavings(int savings) {
        this.savings = savings;
    }

    public int getEmergency() {
        return emergency;
    }

    public void setEmergency(int emergency) {
        this.emergency = emergency;
    }

    public int getNormal() {
        return normal;
    }

    public void setNormal(int normal) {
        this.normal = normal;
    }

    public String getReceiptsavings() {
        return receiptsavings;
    }

    public void setReceiptsavings(String receiptsavings) {
        this.receiptsavings = receiptsavings;
    }

    public String getReceiptEmergency() {
        return receiptEmergency;
    }

    public void setReceiptEmergency(String receiptEmergency) {
        this.receiptEmergency = receiptEmergency;
    }

    public String getReceiptNormal() {
        return receiptNormal;
    }

    public void setReceiptNormal(String receiptNormal) {
        this.receiptNormal = receiptNormal;
    }
    
    
}
