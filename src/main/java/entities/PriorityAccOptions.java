/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author francis
 */
@Entity
@Table(name = "priority_acc_options")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PriorityAccOptions.findAll", query = "SELECT p FROM PriorityAccOptions p"),
    @NamedQuery(name = "PriorityAccOptions.findById", query = "SELECT p FROM PriorityAccOptions p WHERE p.id = :id"),
    @NamedQuery(name = "PriorityAccOptions.findByOption", query = "SELECT p FROM PriorityAccOptions p WHERE p.priorityOption = :priorityOption")})
public class PriorityAccOptions implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "priorityOption")
    private String priorityOption;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "priorityAccount")
    private Collection<Members> membersCollection;

    public PriorityAccOptions() {
    }

    public PriorityAccOptions(Integer id) {
        this.id = id;
    }

    public PriorityAccOptions(Integer id, String priorityOption) {
        this.id = id;
        this.priorityOption = priorityOption;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOption() {
        return priorityOption;
    }

    public void setOption(String priorityOption) {
        this.priorityOption = priorityOption;
    }

    @XmlTransient
    public Collection<Members> getMembersCollection() {
        return membersCollection;
    }

    public void setMembersCollection(Collection<Members> membersCollection) {
        this.membersCollection = membersCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PriorityAccOptions)) {
            return false;
        }
        PriorityAccOptions other = (PriorityAccOptions) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.PriorityAccOptions[ id=" + id + " ]";
    }
    
}
