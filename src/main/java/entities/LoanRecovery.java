/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author francis
 */
@Entity
@Table(name = "loan_recovery")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LoanRecovery.findAll", query = "SELECT l FROM LoanRecovery l"),
    @NamedQuery(name = "LoanRecovery.findById", query = "SELECT l FROM LoanRecovery l WHERE l.id = :id"),
    @NamedQuery(name = "LoanRecovery.findByAmount", query = "SELECT l FROM LoanRecovery l WHERE l.amount = :amount"),
    @NamedQuery(name = "LoanRecovery.findByDateWhen", query = "SELECT l FROM LoanRecovery l WHERE l.dateWhen = :dateWhen")})
public class LoanRecovery implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "amount")
    private int amount;
    @Basic(optional = false)
    @NotNull
    @Column(name = "date_when")
    @Temporal(TemporalType.DATE)
    private Date dateWhen;
    @JoinColumn(name = "loan_id", referencedColumnName = "loan_id")
    @ManyToOne(optional = false)
    private Loan loanId;
    @JoinColumn(name = "member_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Members memberId;

    public LoanRecovery() {
    }

    public LoanRecovery(Integer id) {
        this.id = id;
    }

    public LoanRecovery(Integer id, int amount, Date dateWhen) {
        this.id = id;
        this.amount = amount;
        this.dateWhen = dateWhen;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public Date getDateWhen() {
        return dateWhen;
    }

    public void setDateWhen(Date dateWhen) {
        this.dateWhen = dateWhen;
    }

    public Loan getLoanId() {
        return loanId;
    }

    public void setLoanId(Loan loanId) {
        this.loanId = loanId;
    }

    public Members getMemberId() {
        return memberId;
    }

    public void setMemberId(Members memberId) {
        this.memberId = memberId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LoanRecovery)) {
            return false;
        }
        LoanRecovery other = (LoanRecovery) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.LoanRecovery[ id=" + id + " ]";
    }
    
}
