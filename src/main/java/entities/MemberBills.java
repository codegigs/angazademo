/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.eclipse.persistence.annotations.Cache;

/**
 *
 * @author francis
 */
@Cache(alwaysRefresh = true)
@Entity
@Table(name = "member_bills")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MemberBills.findAll", query = "SELECT m FROM MemberBills m"),
    @NamedQuery(name = "MemberBills.findById", query = "SELECT m FROM MemberBills m WHERE m.id = :id"),
    @NamedQuery(name = "MemberBills.findByAmount", query = "SELECT m FROM MemberBills m WHERE m.amount = :amount"),
    @NamedQuery(name = "MemberBills.findByNarration", query = "SELECT m FROM MemberBills m WHERE m.narration = :narration"),
    @NamedQuery(name = "MemberBills.findByDateIncured", query = "SELECT m FROM MemberBills m WHERE m.dateIncured = :dateIncured")})
public class MemberBills implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "random_int")
    private int randomInt;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "amount")
    private int amount;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "narration")
    private String narration;
    @Basic(optional = false)
    @NotNull
    @Column(name = "date_incured")
    @Temporal(TemporalType.DATE)
    private Date dateIncured;
    @JoinColumn(name = "member_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Members memberId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "billId")
    private Collection<BillTransactions> billTransactionsCollection;

    public MemberBills() {
    }

    public MemberBills(Integer id) {
        this.id = id;
    }

    public MemberBills(Integer id, int amount, String narration, Date dateIncured) {
        this.id = id;
        this.amount = amount;
        this.narration = narration;
        this.dateIncured = dateIncured;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getNarration() {
        return narration;
    }

    public void setNarration(String narration) {
        this.narration = narration;
    }

    public Date getDateIncured() {
        return dateIncured;
    }

    public void setDateIncured(Date dateIncured) {
        this.dateIncured = dateIncured;
    }

    public Members getMemberId() {
        return memberId;
    }

    public void setMemberId(Members memberId) {
        this.memberId = memberId;
    }

    @XmlTransient
    public Collection<BillTransactions> getBillTransactionsCollection() {
        return billTransactionsCollection;
    }

    public void setBillTransactionsCollection(Collection<BillTransactions> billTransactionsCollection) {
        this.billTransactionsCollection = billTransactionsCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MemberBills)) {
            return false;
        }
        MemberBills other = (MemberBills) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.MemberBills[ id=" + id + " ]";
    }

    public int getRandomInt() {
        return randomInt;
    }

    public void setRandomInt(int randomInt) {
        this.randomInt = randomInt;
    }
    
    public int getNetAcc()
    {
        int totalDebit=0;
        int totalCredit=0;
        for(BillTransactions bt:getBillTransactionsCollection())
        {
            totalCredit+=bt.getAmountCredit();
            totalDebit+=bt.getAmountDebit()*-1;
        }
        return totalCredit-totalDebit;
    }
}
