package myUtils;

public class FiguresToWordConverter
{
  private static final String[] units = { "Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen" };
  private static final String[] tens = { "", "", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety" };
  
  public static String convert(Integer i)
  {
    if (i.intValue() < 20) {
      return units[i.intValue()];
    }
    if (i.intValue() < 100) {
      return tens[(i.intValue() / 10)] + (i.intValue() % 10 > 0 ? " " + convert(Integer.valueOf(i.intValue() % 10)) : "");
    }
    if (i.intValue() < 1000) {
      return units[(i.intValue() / 100)] + " Hundred" + (i.intValue() % 100 > 0 ? " and " + convert(Integer.valueOf(i.intValue() % 100)) : "");
    }
    if (i.intValue() < 1000000) {
      return convert(Integer.valueOf(i.intValue() / 1000)) + " Thousand " + (i.intValue() % 1000 > 0 ? " " + convert(Integer.valueOf(i.intValue() % 1000)) : "");
    }
    return convert(Integer.valueOf(i.intValue() / 1000000)) + " Million " + (i.intValue() % 1000000 > 0 ? " " + convert(Integer.valueOf(i.intValue() % 1000000)) : "");
  }
}
