/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facades;

import entities.BillTransactions;
import entities.CollectionTypes;
import entities.Collections;
import entities.DepositCash;
import entities.Members;
import entities.SaccoLedgerEntries;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author francis
 */
@Stateless
public class CollectionsFacade extends AbstractFacade<Collections> {
    @PersistenceContext(unitName = "SaccoManagement")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CollectionsFacade() {
        super(Collections.class);
    }

    public void persistDeposit(DepositCash depo) {
        getEntityManager().persist(depo);
    }

    public void storeBillTransaction(BillTransactions bt) {
        getEntityManager().persist(bt);
    }

    public void saveLedgerEntry(SaccoLedgerEntries sle) {
        getEntityManager().persist(sle);
    }
    
    public List<Collections> findAllItems() {
        return getEntityManager().createNamedQuery("Collections.findAll").getResultList();
    }
    
    public List<Collections> findItemsByDate(Date toDate) {
        return getEntityManager().createNamedQuery("Collections.findByDateCollected").setParameter("dateCollected", toDate).getResultList();
    }

    public List<Collections> findItemsByDate(Date toDate, CollectionTypes cl) {
        return getEntityManager().createNamedQuery("Collections.findByDateCollectedAndType").setParameter("dateCollected", toDate).setParameter("type", cl).getResultList();
    }

    public CollectionTypes findCollectionType(int i) {
        return (CollectionTypes) getEntityManager().createNamedQuery("CollectionTypes.findById").setParameter("id", i).getSingleResult();
    }

    public Collections getByRandom(int randomInt) {
        return (Collections) getEntityManager().createQuery("SELECT c FROM Collections c WHERE c.randomInt = :rand").setParameter("rand", randomInt).getSingleResult();
    }

    public Members findMember(int memberId) {
        return getEntityManager().find(Members.class, memberId);
    }
    
    public Iterable<Collections> getMemberUnbilledCollections(Members m) {
        getEntityManager().flush();
        return getEntityManager().createQuery("SELECT c FROM Collections c WHERE c.statusBilled=:billed AND c.memberId=:id").setParameter("billed", false).setParameter("id", m).getResultList();
    }
    
}
