/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facades;

import entities.ExpensesCategory;
import entities.SaccoLedgers;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author francis
 */
@Stateless
public class SaccoLedgersFacade extends AbstractFacade<SaccoLedgers> {
    @PersistenceContext(unitName = "SaccoManagement")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public SaccoLedgersFacade() {
        super(SaccoLedgers.class);
    }

    public void storeE(ExpensesCategory exp) {
        getEntityManager().persist(exp);
    }

    public void editSl(SaccoLedgers sl) {
        getEntityManager().merge(sl);
    }
    
}
