/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facades;

import entities.PaymentSummary;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author francis
 */
@Stateless
public class PaymentSummaryFacade extends AbstractFacade<PaymentSummary> {
    @PersistenceContext(unitName = "SaccoManagement")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PaymentSummaryFacade() {
        super(PaymentSummary.class);
    }

    public List<PaymentSummary> findAllOrderedById() {
        return getEntityManager().createQuery("SELECT p FROM PaymentSummary p ORDER BY p.id DESC").getResultList();
    }
    
}
